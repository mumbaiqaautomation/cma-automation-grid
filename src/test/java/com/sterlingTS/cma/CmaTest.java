package com.sterlingTS.cma;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.apache.http.util.EntityUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sterling.cma.model.CmaRequest;
import com.sterling.cma.model.CmaResources;
import com.sterling.cma.model.CrimCase;
import com.sterling.cma.model.CrimCharge;
import com.sterling.cma.model.constant.CmaConstant.ExcellConstant;
import com.sterling.cma.utils.ExcelProcessor;
import com.sterling.cma.utils.ExcelWrite;

public class CmaTest {

	/*@DataProvider(name = "Passing List Of List")
	public List<List<String>> dataSetRetrive() throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		return ExcelProcessor.getExcelData();
	}*/
	
	@Test
	public void cmaRuleExecuteTest()throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		List<List<String>> testData = ExcelProcessor.getExcelData();
		List<List<String>> testOutData = new ArrayList<List<String>>();
		RestTemplate restTemplate = new RestTemplate();
		
		String url = "https://cma.pre.us.backgroundcheck.com/v1/criminal-cma";
		
		for(int i=0;i<testData.size();i++)
		{
			ObjectMapper mapper = new ObjectMapper();
			CmaResources cmaResponse = null;
			List<String> singleRowData = testData.get(i);
			System.out.println(singleRowData);
			System.out.println(singleRowData.size());
			CmaRequest cmarequest = getCMARequest(singleRowData);
			System.out.println(cmarequest.toString());
			final HttpEntity<CmaRequest> request = new HttpEntity<>(cmarequest);
			final ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
			
			System.out.println(response.getBody().toString());
			System.out.println(response.getStatusCodeValue());
			
			if(response != null){
				cmaResponse = mapper.readValue(response.getBody().toString(), CmaResources.class);
			}
			String expectedLevel = singleRowData.get(ExcellConstant.COL_INDEX_EXPECTED_LEVEL);
			String expectedCmaType = singleRowData.get(ExcellConstant.COL_INDEX_EXPECTED_CMATYPE);
			singleRowData.remove(ExcellConstant.COL_INDEX_EXPECTED_LEVEL);
			singleRowData.remove(ExcellConstant.COL_INDEX_EXPECTED_LEVEL);
			singleRowData.add(expectedLevel);
			singleRowData.add(cmaResponse.getCmaResponse().getScore());
			singleRowData.add(expectedCmaType);
			if(cmaResponse.getCmaResponse().getCrimCases().iterator().next().getCharges().iterator().next().getCmaType() == null)
			{
				singleRowData.add("");
			}else{
			singleRowData.add(cmaResponse.getCmaResponse().getCrimCases().iterator().next().getCharges().iterator().next().getCmaType());
			}
			System.out.println(cmaResponse.getCmaResponse().getCrimCases().iterator().next().getCharges().iterator().next().getCmaType());
			System.out.println(singleRowData.get(ExcellConstant.COL_INDEX_EXPECTED_CMATYPE+1));
			
			if(cmaResponse.getCmaResponse().getCrimCases().iterator().next().getCharges().iterator().next().getCmaType() == null)
			{
				if(cmaResponse.getCmaResponse().getScore().equalsIgnoreCase(singleRowData.get(ExcellConstant.COL_INDEX_EXPECTED_LEVEL)) && "".equalsIgnoreCase(singleRowData.get(ExcellConstant.COL_INDEX_EXPECTED_CMATYPE+1)))
				{
					singleRowData.add("PASS");
				}else{
					singleRowData.add("FAIL");
				}
			}else{
				if(cmaResponse.getCmaResponse().getScore().equalsIgnoreCase(singleRowData.get(ExcellConstant.COL_INDEX_EXPECTED_LEVEL)) && cmaResponse.getCmaResponse().getCrimCases().iterator().next().getCharges().iterator().next().getCmaType().equalsIgnoreCase(singleRowData.get(ExcellConstant.COL_INDEX_EXPECTED_CMATYPE+1)))
				{
					singleRowData.add("PASS");
				}else{
					singleRowData.add("FAIL");
				}
			}
			System.out.println(singleRowData);
			
			testOutData.add(singleRowData);
			//singleRowData.clear();
		}
		
		ExcelWrite.writeDataToExcel(testOutData);
	}
	
	public CmaRequest getCMARequest(List<String> testDataSet)
	{
		CmaRequest cmaReq = new CmaRequest();
		cmaReq.setCmaRequestId(0L);
		cmaReq.setModelId(testDataSet.get(ExcellConstant.COL_INDEX_MODELID));
		cmaReq.setCmaTrackingId(null);
		cmaReq.setCases(createCrimCaces(testDataSet));
		return cmaReq;
	}
	
	private Set<CrimCase> createCrimCaces(List<String> rowList) {
		Set<CrimCase> crimCases = new HashSet<CrimCase>();
				crimCases.add(createSingleCaseMultipleCharge(rowList));
		return crimCases;
	}
	
	private CrimCase createSingleCaseMultipleCharge(List<String> rowList) {
		CrimCase crimCase = createSingleCase(null);
		crimCase.setCrimCaseId(0L);
		crimCase.setNumber(RandomStringUtils.randomNumeric(6));
		crimCase.setCharges(createMultipleCharges(rowList));
		return crimCase;
	}
	
	private CrimCase createSingleCase(Row row) {
		CrimCase crimCase = new CrimCase();
		if (row != null) {
			crimCase.setCrimCaseId(0L);
			crimCase.setNumber(RandomStringUtils.randomNumeric(6));
		}
		return crimCase;
	}
	
	private Set<CrimCharge> createMultipleCharges(List<String> rowList) {
		Set<CrimCharge> charges = new HashSet<CrimCharge>();
		
			charges.add(createSingleCharge(rowList));
		
		return charges;
	}
	
	private CrimCharge createSingleCharge(List<String> rowList) {
		CrimCharge charge = new CrimCharge();
		charge.setCrimChargeId(0L);
		charge.setNumber(RandomStringUtils.randomNumeric(6));
		
		if(!rowList.get(10).isEmpty())
		{
			charge.setChargeCategory(rowList.get(10));
		}else if(!rowList.get(11).isEmpty())
		{
			charge.setChargeCategory(rowList.get(11));
		}else if(!rowList.get(12).isEmpty())
		{
			charge.setChargeCategory(rowList.get(12));
		}
		//charge.setChargeCategory(getCellValue(row, ExcellConstant.COL_INDEX_CHARGE_CATEGORY));
		// charge.setChargeLevel(getCellValue(row,
		// ExcellConstant.COL_INDEX_CHARGE_LEVEL));
		charge.setChargeLevel(rowList.get(17));
		charge.setDispositionType(rowList.get(ExcellConstant.COL_INDEX_DISPOSITION_TYPE));
		charge.setDisposition(rowList.get(ExcellConstant.COL_INDEX_DISPOSITION));
		charge.setAgeOfRecord(rowList.get(ExcellConstant.COL_INDEX_AGE_OF_RECORD));
		charge.setAgeOfSentence(rowList.get(ExcellConstant.COL_INDEX_AGE_OF_SENTENCE));
		String ageOfSentenceProbationOperator = rowList.get(
				ExcellConstant.COL_INDEX_AGE_OF_SENTENCE_PROBATION_OPERATOR);
		if (!rowList.get( ExcellConstant.COL_INDEX_AGE_OF_SENTENCE_PROBATION).isEmpty()) {
			Double ageOfSentenceProbation = Double
					.valueOf(rowList.get( ExcellConstant.COL_INDEX_AGE_OF_SENTENCE_PROBATION));
			if (!ageOfSentenceProbationOperator.isEmpty() && ageOfSentenceProbationOperator.equalsIgnoreCase("gt")
					&& ageOfSentenceProbation != null) {
				ageOfSentenceProbation = ageOfSentenceProbation + 1;
			} else {
				ageOfSentenceProbation = ageOfSentenceProbation;
			}
			charge.setAgeofSentenceProbation(ageOfSentenceProbation.toString());
		}
		charge.setWarrantStatus(rowList.get(ExcellConstant.COL_INDEX_WARRANT_ACTIVE));
		charge.setSentenceStatus(rowList.get(ExcellConstant.COL_INDEX_SENTENCE_ACTIVE));
		charge.setState(rowList.get(ExcellConstant.COL_INDEX_STATE));
		charge.setProbationStatus(rowList.get(ExcellConstant.COL_INDEX_PROBATION_STATUS));
		charge.setDefendantAge(rowList.get(ExcellConstant.COL_INDEX_PROBATION_STATUS));
		return charge;
	}
}
