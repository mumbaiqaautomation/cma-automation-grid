package com.sterling.cma.utils;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelWrite {
	
	public static XSSFWorkbook workbook = new XSSFWorkbook();
	public static XSSFSheet sheet ;
	 
	public static void writeDataToExcel(List<List<String>> testOutData) throws IOException
	{
		FileOutputStream fileOut = new FileOutputStream("CMARuleOutput.xlsx");
		sheet = workbook.createSheet("CMAOutPut");
		/*createHeaderInExcel(sheet, fileOut);
		writeDataToExcelTo(sheet, testOutData);*/
		
		XSSFCellStyle my_style = workbook.createCellStyle();
		my_style.setBorderLeft(XSSFCellStyle.BORDER_THICK);
		my_style.setBorderRight(XSSFCellStyle.BORDER_THICK);
		my_style.setBorderTop(XSSFCellStyle.BORDER_THICK);
		my_style.setBorderBottom(XSSFCellStyle.BORDER_THICK);
		my_style.setFillForegroundColor(IndexedColors.BLUE.getIndex());
		my_style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		
		Row header = sheet.createRow(0);
		
		for(int i=0;i<ExcelProcessor.headerData.size()-1;i++)
		{
			Cell cell_slno = header.createCell(i);
			cell_slno.setCellValue(ExcelProcessor.headerData.get(i));
			cell_slno.setCellStyle(my_style);
		}
		Cell cell_ActualLevel = header.createCell(ExcelProcessor.headerData.size()-1);
		Cell cell_ExpectedCMAType = header.createCell(ExcelProcessor.headerData.size());
		Cell cell_ActualCMAType = header.createCell(ExcelProcessor.headerData.size()+1);
		Cell cell_Status = header.createCell(ExcelProcessor.headerData.size()+2);
		cell_ActualLevel.setCellValue("Actual Level");
		cell_ActualLevel.setCellStyle(my_style);
		cell_ExpectedCMAType.setCellValue("Expected CMA Type");
		cell_ExpectedCMAType.setCellStyle(my_style);
		cell_ActualCMAType.setCellValue("Actual CMA Type");
		cell_ActualCMAType.setCellStyle(my_style);
		cell_Status.setCellValue("Status");
		cell_Status.setCellStyle(my_style);
		
		int rowCount = 1;
		for(int i=0;i<testOutData.size();i++)
		{
			Row row = sheet.createRow(rowCount++);
			List<String> singleData = testOutData.get(i);
			for(int j=0;j<singleData.size();j++)
			{
				row.createCell(j).setCellValue(singleData.get(j));
			}
		}
		
		
		workbook.write(fileOut);
        fileOut.close();
        workbook.close();
	}
	


}
