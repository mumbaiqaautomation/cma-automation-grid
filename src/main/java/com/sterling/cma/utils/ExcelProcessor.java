package com.sterling.cma.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ExcelProcessor {
	
	public static List<String> headerData = new ArrayList<String>();

	private static List<List<String>> readDataFromExcel() throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		List<String> allCharges = new ArrayList<String>();
		List<List<String>> dataSet = new ArrayList<List<String>>();
		Workbook workbook = WorkbookFactory.create(new File("CmaRules.xlsx"));
		Iterator<Sheet> sheetIterator = workbook.sheetIterator();
        System.out.println("Retrieving Sheets using Iterator");
        while (sheetIterator.hasNext()) {
            Sheet sheet = sheetIterator.next();
            System.out.println("=> " + sheet.getSheetName());
        }
        Sheet sheet = workbook.getSheetAt(0);
        DataFormatter dataFormatter = new DataFormatter();
        for (Row row: sheet) {
        	List<String> allCmaRules = new ArrayList<String>();
        	allCharges.clear();
            for(Cell cell: row) {
                String cellValue = dataFormatter.formatCellValue(cell);
                System.out.print(cellValue + "\t");
                allCharges.add(cellValue);
                allCmaRules.add(cellValue);
            }
            dataSet.add(allCmaRules);
            System.out.println();
        }
        
        return dataSet;
	}
	
	public static List<List<String>> getExcelData() throws EncryptedDocumentException, InvalidFormatException, IOException
	{
		List<List<String>> dataset = readDataFromExcel();
		headerData = dataset.get(0);
		dataset.remove(0);
		return dataset;
	}
	
}
