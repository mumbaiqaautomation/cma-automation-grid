package com.sterling.cma.model;

public class CmaRequestDaoMapper {
	private String customer;
	private String account;
	private String profile;
	private String productCategory;
	private String parentSystem;
	
	
	private String modelNumber;
	private String chargeLevel;
	private String groupChargeLevel;
	private String dispositionType;
	private String disposition;
	private String exceptDisposition;
	private String chargeCategory;
	private String groupChargeCategory;
	private String exceptChargeCategory;
	private String warrantStatus;
	private String ageOfRecord;
	private String ageOfRecordOpt;
	// private String categoryLevel;
	private String state;
	private String exceptState;
	// private String category;
	private String ageOfSentence;
	private String ageOfSentenceOpt;
	private String sentenceStatus;
	private String probationStatus;
	private String defendantAge;
	private String cmaLevel;
	private String frequency;
	private String frequencyOpt;
	private String logicalOperation;
	private String cmaTire;
	private String cmaType;
	private String ageOfSentenceProbation;
	private String ageOfSentenceProbationOpt;

	public String getCmaType() {
		return cmaType;
	}

	public void setCmaType(String cmaType) {
		this.cmaType = cmaType;
	}

	public String getAgeOfRecordOpt() {
		return ageOfRecordOpt;
	}

	public void setAgeOfRecordOpt(String ageOfRecordOpt) {
		this.ageOfRecordOpt = ageOfRecordOpt;
	}

	public String getAgeOfSentenceOpt() {
		return ageOfSentenceOpt;
	}

	public void setAgeOfSentenceOpt(String ageOfSentenceOpt) {
		this.ageOfSentenceOpt = ageOfSentenceOpt;
	}

	public String getFrequencyOpt() {
		return frequencyOpt;
	}

	public void setFrequencyOpt(String frequencyOpt) {
		this.frequencyOpt = frequencyOpt;
	}

	public String getAgeOfSentenceProbation() {
		return ageOfSentenceProbation;
	}

	public void setAgeOfSentenceProbation(String ageOfSentenceProbation) {
		this.ageOfSentenceProbation = ageOfSentenceProbation;
	}

	public String getAgeOfSentenceProbationOpt() {
		return ageOfSentenceProbationOpt;
	}

	public void setAgeOfSentenceProbationOpt(String ageOfSentenceProbationOpt) {
		this.ageOfSentenceProbationOpt = ageOfSentenceProbationOpt;
	}

	public String getCmaTire() {
		return cmaTire;
	}

	public void setCmaTire(String cmaTire) {
		this.cmaTire = cmaTire;
	}

	public String getModelNumber() {
//		for team testing purpose
		
		StringBuffer modelNumberBuf = new StringBuffer();
		modelNumberBuf.append((customer != null && customer.length() > 0)? customer + "|": null);
		modelNumberBuf.append((account != null && account.length() > 0)? account.trim() + "|": "|");
		System.out.println("|"+profile.trim()+"|");
		modelNumberBuf.append((profile != null && profile.length() > 0)? profile.trim() + "|": "|");
		modelNumberBuf.append((productCategory != null && productCategory.length() > 0)? productCategory + "|": null);
		modelNumberBuf.append((parentSystem != null && parentSystem.length() > 0)? parentSystem : null);
		modelNumber = modelNumberBuf.toString();
		return modelNumber;
	}

	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	public String getChargeLevel() {
		return chargeLevel;
	}

	public void setChargeLevel(String chargeLevel) {
		this.chargeLevel = chargeLevel;
	}

	public String getGroupChargeLevel() {
		return groupChargeLevel;
	}

	public void setGroupChargeLevel(String groupChargeLevel) {
		this.groupChargeLevel = groupChargeLevel;
	}

	public String getDispositionType() {
		return dispositionType;
	}

	public void setDispositionType(String dispositionType) {
		this.dispositionType = dispositionType;
	}

	public String getDisposition() {
		return disposition;
	}

	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}

	public String getExceptDisposition() {
		return exceptDisposition;
	}

	public void setExceptDisposition(String exceptDisposition) {
		this.exceptDisposition = exceptDisposition;
	}

	public String getChargeCategory() {
		return chargeCategory;
	}

	public void setChargeCategory(String chargeCategory) {
		this.chargeCategory = chargeCategory;
	}

	public String getGroupChargeCategory() {
		return groupChargeCategory;
	}

	public void setGroupChargeCategory(String groupChargeCategory) {
		this.groupChargeCategory = groupChargeCategory;
	}

	public String getExceptChargeCategory() {
		return exceptChargeCategory;
	}

	public void setExceptChargeCategory(String exceptChargeCategory) {
		this.exceptChargeCategory = exceptChargeCategory;
	}

	public String getWarrantStatus() {
		return warrantStatus;
	}

	public void setWarrantStatus(String warrantStatus) {
		this.warrantStatus = warrantStatus;
	}

	public String getAgeOfRecord() {
		return ageOfRecord;
	}

	public void setAgeOfRecord(String ageOfRecord) {
		this.ageOfRecord = ageOfRecord;
	}

	/*
	 * public String getCategoryLevel() { return categoryLevel; }
	 * 
	 * public void setCategoryLevel(String categoryLevel) { this.categoryLevel =
	 * categoryLevel; }
	 */

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getExceptState() {
		return exceptState;
	}

	public void setExceptState(String exceptState) {
		this.exceptState = exceptState;
	}

	/*
	 * public String getCategory() { return category; }
	 * 
	 * public void setCategory(String category) { this.category = category; }
	 */

	public String getAgeOfSentence() {
		return ageOfSentence;
	}

	public void setAgeOfSentence(String ageOfSentence) {
		this.ageOfSentence = ageOfSentence;
	}

	public String getSentenceStatus() {
		return sentenceStatus;
	}

	public void setSentenceStatus(String sentenceStatus) {
		this.sentenceStatus = sentenceStatus;
	}

	public String getProbationStatus() {
		return probationStatus;
	}

	public void setProbationStatus(String probationStatus) {
		this.probationStatus = probationStatus;
	}

	public String getDefendantAge() {
		return defendantAge;
	}

	public void setDefendantAge(String defendantAge) {
		this.defendantAge = defendantAge;
	}

	public String getCmaLevel() {
		return cmaLevel;
	}

	public void setCmaLevel(String cmaLevel) {
		this.cmaLevel = cmaLevel;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getLogicalOperation() {
		return logicalOperation;
	}

	public void setLogicalOperation(String logicalOperation) {
		this.logicalOperation = logicalOperation;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	public String getParentSystem() {
		return parentSystem;
	}

	public void setParentSystem(String parentSystem) {
		this.parentSystem = parentSystem;
	}

	@Override
	public String toString() {
		return "CmaRequestDaoMapper [customer=" + customer + ", account=" + account + ", profile=" + profile
				+ ", productCategory=" + productCategory + ", parentSystem=" + parentSystem + ", modelNumber="
				+ modelNumber + ", chargeLevel=" + chargeLevel + ", groupChargeLevel=" + groupChargeLevel
				+ ", dispositionType=" + dispositionType + ", disposition=" + disposition + ", exceptDisposition="
				+ exceptDisposition + ", chargeCategory=" + chargeCategory + ", groupChargeCategory="
				+ groupChargeCategory + ", exceptChargeCategory=" + exceptChargeCategory + ", warrantStatus="
				+ warrantStatus + ", ageOfRecord=" + ageOfRecord + ", ageOfRecordOpt=" + ageOfRecordOpt + ", state="
				+ state + ", exceptState=" + exceptState + ", ageOfSentence=" + ageOfSentence + ", ageOfSentenceOpt="
				+ ageOfSentenceOpt + ", sentenceStatus=" + sentenceStatus + ", probationStatus=" + probationStatus
				+ ", defendantAge=" + defendantAge + ", cmaLevel=" + cmaLevel + ", frequency=" + frequency
				+ ", frequencyOpt=" + frequencyOpt + ", logicalOperation=" + logicalOperation + ", cmaTire=" + cmaTire
				+ ", cmaType=" + cmaType + ", ageOfSentenceProbation=" + ageOfSentenceProbation
				+ ", ageOfSentenceProbationOpt=" + ageOfSentenceProbationOpt + "]";
	}
	
}
