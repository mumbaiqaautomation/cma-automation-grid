package com.sterling.cma.model;

import java.util.HashSet;
import java.util.Set;

import org.springframework.util.CollectionUtils;

public class GroupChargeCategory {
	private String groupChargeCategoryName;
	private Set<GroupChargeCategory> groupChargeCategoryList;
	private Set<String> chargeCategoryList;

	public String getGroupChargeCategoryName() {
		return groupChargeCategoryName;
	}

	public void setGroupChargeCategoryName(String groupChargeCategoryName) {
		this.groupChargeCategoryName = groupChargeCategoryName;
	}

	public Set<GroupChargeCategory> getGroupChargeCategoryList() {
		if(CollectionUtils.isEmpty(groupChargeCategoryList)){
			groupChargeCategoryList = new HashSet<>();
		}
		return groupChargeCategoryList;
	}

	public void setGroupChargeCategoryList(Set<GroupChargeCategory> groupChargeCategoryList) {
		this.groupChargeCategoryList = groupChargeCategoryList;
	}

	public Set<String> getChargeCategoryList() {
		if(CollectionUtils.isEmpty(chargeCategoryList)){
			chargeCategoryList = new HashSet<>();
		}
		return chargeCategoryList;
	}

	public void setChargeCategoryList(Set<String> chargeCategoryList) {
		this.chargeCategoryList = chargeCategoryList;
	}

}
