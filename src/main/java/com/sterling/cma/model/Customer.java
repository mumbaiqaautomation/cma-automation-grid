package com.sterling.cma.model;

import java.util.Set;

public class Customer {
	private int customerId;
	private String customerName;
	private ParentSystem parentSystem;
	private String customerDescription;
	private Set<Account> accounts;
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public ParentSystem getParentSystem() {
		return parentSystem;
	}
	public void setParentSystem(ParentSystem parentSystem) {
		this.parentSystem = parentSystem;
	}
	public String getCustomerDescription() {
		return customerDescription;
	}
	public void setCustomerDescription(String customerDescription) {
		this.customerDescription = customerDescription;
	}
	public Set<Account> getAccounts() {
		return accounts;
	}
	public void setAccounts(Set<Account> accounts) {
		this.accounts = accounts;
	}

}
