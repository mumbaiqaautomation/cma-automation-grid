package com.sterling.cma.model;

import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

public class CMATypeWarrantStatus extends AbstractCMAType{
	
	

	public CMATypeWarrantStatus(CMAType inner) {
		super(inner);
		// TODO Auto-generated constructor stub
	}
	
	public Collection<Predicate<CrimCharge>>  score(Collection<Predicate<CrimCharge>> filters ,Collection<Predicate<CrimCharge>> 
	exceptionfilters,
	Collection<Predicate<CrimCharge>> groupChargelevelFilter,Collection<Predicate<CrimCharge>> dispositionFilter,Collection<Predicate<CrimCharge>> stateFilter,CmaRule cmaRule) {
		
		super.score(filters,exceptionfilters,groupChargelevelFilter,dispositionFilter,stateFilter,cmaRule);
		doDispostionType(filters,cmaRule);
		return null;
		
	}

	private void doDispostionType(Collection<Predicate<CrimCharge>> filters,CmaRule cmaRule) {
		// TODO Auto-generated method stub
		
		Predicate<CrimCharge> dispPredicate = p->{
			String dispositionPredicate = p.getWarrantStatus();
			return dispositionPredicate!= null && p.getWarrantStatus().equalsIgnoreCase(cmaRule.getWarrantStatus());
		};
		filters.add(dispPredicate);
		
	}

}
