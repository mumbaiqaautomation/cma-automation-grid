package com.sterling.cma.model;

import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CMATypeDispositionType extends AbstractCMAType {

	private static Logger logger = LoggerFactory.getLogger(CMATypeDispositionType.class);

	public CMATypeDispositionType(CMAType inner) {
		super(inner);
		// TODO Auto-generated constructor stub
	}

	public Collection<Predicate<CrimCharge>> score(Collection<Predicate<CrimCharge>> filters,
			Collection<Predicate<CrimCharge>> exceptionfilters,
			Collection<Predicate<CrimCharge>> groupChargelevelFilter,
			Collection<Predicate<CrimCharge>> dispositionFilter, Collection<Predicate<CrimCharge>> stateFilter,CmaRule cmaRule) {

		super.score(filters, exceptionfilters, groupChargelevelFilter, dispositionFilter, stateFilter,cmaRule);
		doDispostionType(filters, dispositionFilter, cmaRule);
		return null;

	}

	private void doDispostionType(Collection<Predicate<CrimCharge>> filters,
			Collection<Predicate<CrimCharge>> dispositionFilter, CmaRule cmaRule) {
		// TODO Auto-generated method stub
		// logger.info("setting filter for dispType"+ cmaRule.getDispositionType());
		if (cmaRule.getDispositionType() != null) {
			String[] dispositionTypes = cmaRule.getDispositionType().split(",");
			for (String dispositionType : dispositionTypes) {
				Predicate<CrimCharge> dispPredicate = p -> {
					boolean r = true;
					String dispositionPredicate = p.getDispositionType();
					if (null != dispositionPredicate && dispositionType != null) {
						if (dispositionPredicate.equalsIgnoreCase("Active")
								|| dispositionPredicate.equalsIgnoreCase("Pending")) {
							r = (dispositionType.trim().equalsIgnoreCase(dispositionPredicate)
									|| dispositionType.trim().equalsIgnoreCase(dispositionPredicate));
						} else {
							r = dispositionType.trim().equalsIgnoreCase(dispositionPredicate);
						}

					}

					return r;
				};
				// filters.add(dispPredicate);
				dispositionFilter.add(dispPredicate);
			}

		}

	}

}
