package com.sterling.cma.model.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.sterling.cma.model.ChargeLevel;

class ChargeLevelWeightage{
	public static final int FELONY_WEIGHT=12;
	public static final int MISDEMEANOR_WEIGHT=10;
	public static final int VIOLATION_WEIGHT=1;
	public static final int UNKNOWN_WEIGHT=0;
}

public enum ChargeLevelEnum {
	
	  FELONY( 1, "FELONY",      "Felony",MappedChargeLevelEnum.FELONY,ChargeLevelWeightage.FELONY_WEIGHT),
	  MISDEMEANOR( 2, "MISDEMEANOR", "Misdemeanor",MappedChargeLevelEnum.MISDEMEANOR,ChargeLevelWeightage.MISDEMEANOR_WEIGHT),
	  VIOLATION( 3, "VIOLATION",   "Violation",MappedChargeLevelEnum.VIOLATION,ChargeLevelWeightage.VIOLATION_WEIGHT),
	  UNKNOWN  ( 4, "UNKNOWN",     "Unknown",MappedChargeLevelEnum.UNKNOWN,ChargeLevelWeightage.UNKNOWN_WEIGHT),
	  DATAENTRY  ( 5, "DATAENTRY",     "DATA ENTRY",MappedChargeLevelEnum.UNKNOWN,ChargeLevelWeightage.UNKNOWN_WEIGHT),
	  GROSS_MISDEMEANOR  ( 6, "GROSS_MISDEMEANOR",     "Gross Misdemeanor",MappedChargeLevelEnum.MISDEMEANOR,ChargeLevelWeightage.MISDEMEANOR_WEIGHT),
	  SERIOUS_MISDEMEANOR  ( 7, "SERIOUS_MISDEMEANOR",     "Serious Misdemeanor",MappedChargeLevelEnum.MISDEMEANOR,ChargeLevelWeightage.MISDEMEANOR_WEIGHT),
	  SIMPLE_MISDEMEANOR  ( 8, "SIMPLE_MISDEMEANOR",     "Simple Misdemeanor",MappedChargeLevelEnum.MISDEMEANOR,ChargeLevelWeightage.MISDEMEANOR_WEIGHT),
	  AGRRAVATED_MISDEMEANOR  ( 9, "AGRRAVATED_MISDEMEANOR",     "Agrravated Misdemeanor",MappedChargeLevelEnum.MISDEMEANOR,ChargeLevelWeightage.MISDEMEANOR_WEIGHT),
	  TRAFFIC  ( 10, "TRAFFIC",     "Traffic",MappedChargeLevelEnum.VIOLATION,ChargeLevelWeightage.VIOLATION_WEIGHT),
	  INFRACTION  ( 11, "INFRACTION",     "Infraction",MappedChargeLevelEnum.VIOLATION,ChargeLevelWeightage.VIOLATION_WEIGHT),
	  NOT_LISTED  ( 12, "NOT_LISTED",     "Not listed",MappedChargeLevelEnum.UNKNOWN,ChargeLevelWeightage.UNKNOWN_WEIGHT),
	  SEALED  ( 13, "SEALED",     "Sealed",MappedChargeLevelEnum.UNKNOWN,ChargeLevelWeightage.UNKNOWN_WEIGHT),
	  SHOW_CAUSE  ( 15, "SHOW_CAUSE",     "Show cause",MappedChargeLevelEnum.UNKNOWN,ChargeLevelWeightage.UNKNOWN_WEIGHT),
	  ORDINANCE_VIOLATION  ( 15, "ORDINANCE_VIOLATION","Ordinance violation",MappedChargeLevelEnum.VIOLATION,ChargeLevelWeightage.VIOLATION_WEIGHT),
	  MUNICIPAL_ORDINANCE  ( 16, "MUNICIPAL_ORDINANCE","Municipal Ordinance",MappedChargeLevelEnum.VIOLATION,ChargeLevelWeightage.VIOLATION_WEIGHT),
	  INDICTABLE  ( 17, "INDICTABLE",     "Indictable",MappedChargeLevelEnum.FELONY,ChargeLevelWeightage.FELONY_WEIGHT),
	  NON_INDICTABLE  ( 18, "NON_INDICTABLE","Non Indictable",MappedChargeLevelEnum.MISDEMEANOR,ChargeLevelWeightage.MISDEMEANOR_WEIGHT),
	  DISORDERLY_PERSON  ( 19, "DISORDERLY_PERSON",     "Disorderly Person",MappedChargeLevelEnum.VIOLATION,ChargeLevelWeightage.VIOLATION_WEIGHT),
	  FORFEITURE  ( 20, "FORFEITURE",     "Forfeiture",MappedChargeLevelEnum.VIOLATION,ChargeLevelWeightage.VIOLATION_WEIGHT),
	  CAPIAS  ( 21, "CAPIAS",     "Capias",MappedChargeLevelEnum.UNKNOWN,ChargeLevelWeightage.UNKNOWN_WEIGHT),
	  BRANCH  ( 22, "BRANCH",     "Branch",MappedChargeLevelEnum.UNKNOWN,ChargeLevelWeightage.UNKNOWN_WEIGHT),
	  PETTY_OFFENSE  ( 23, "PETTY_OFFENSE",     "Petty Offense",MappedChargeLevelEnum.VIOLATION,ChargeLevelWeightage.VIOLATION_WEIGHT),
	  SUMMARY  ( 24, "SUMMARY",     "Summary",MappedChargeLevelEnum.VIOLATION,ChargeLevelWeightage.VIOLATION_WEIGHT),
	  UNDESIGNATED  ( 25, "UNDESIGNATED",     "Undesignated",MappedChargeLevelEnum.UNKNOWN,ChargeLevelWeightage.UNKNOWN_WEIGHT),
	  PETTY_MISDEMEANOR ( 26, "PETTY_MISDEMEANOR",     "Petty Misdemeanor",MappedChargeLevelEnum.MISDEMEANOR,ChargeLevelWeightage.MISDEMEANOR_WEIGHT),
	  SUMMARY_PA ( 27, "SUMMARY_PA",     "Summary PA",MappedChargeLevelEnum.VIOLATION,ChargeLevelWeightage.VIOLATION_WEIGHT),
	  PETTY_MN ( 28, "PETTY_MN",     "Petty MN",MappedChargeLevelEnum.VIOLATION,ChargeLevelWeightage.VIOLATION_WEIGHT),
	  MISCELLANEOUS ( 29, "MISCELLANEOUS",     "Miscellaneous",MappedChargeLevelEnum.VIOLATION,ChargeLevelWeightage.VIOLATION_WEIGHT);

	private static final Map<String, ArrayList<String>> mapData;
    
	static {
    	mapData = new HashMap<String, ArrayList<String>>();
        for (ChargeLevelEnum chargeLevelEnum : ChargeLevelEnum.values()) {
        	String mappedChargeLevelName = chargeLevelEnum.getMappedChargeLevel().getName();
            if(mapData.containsKey(mappedChargeLevelName)){
            	mapData.get(mappedChargeLevelName).add(chargeLevelEnum.getLevel());
            }else{
            	ArrayList<String> dataList = new ArrayList<String>();
            	dataList.add(chargeLevelEnum.getLevel());
            	mapData.put(mappedChargeLevelName, dataList);
            }
        }
    }
	
	public static ArrayList<String> findByMappedChargeLevel(String mappedLevel) {
        return mapData.get(mappedLevel);
    }
	
	private MappedChargeLevelEnum mappedChargeLevel;
	  private int chargeLevelId;
	  private String level;
	  private String chargeLevelDescription;
	  private int weightage;
	  
	  public int getWeightage() {
		return weightage;
	}


	public void setWeightage(int weightage) {
		this.weightage = weightage;
	}


	private ChargeLevelEnum ( int chargeLevelId, String level, String chargeLevelDescription, MappedChargeLevelEnum mappedChargeLevel, int weightage)
	  {
	    this.mappedChargeLevel=mappedChargeLevel;
	    this.chargeLevelId=chargeLevelId;
	    this.chargeLevelDescription=chargeLevelDescription;
	    this.level=level;
	    this.weightage=weightage;
	  }

	 
	  public ChargeLevel getCrimChargeLevel ()
	  {
	    ChargeLevel crimChargeLevel = new ChargeLevel();
	    crimChargeLevel.setLevel ( level );
	    crimChargeLevel.setDescription ( chargeLevelDescription );
	    return crimChargeLevel;
	  }


	public MappedChargeLevelEnum getMappedChargeLevel() {
		return mappedChargeLevel;
	}


	public void setMappedChargeLevel(MappedChargeLevelEnum mappedChargeLevel) {
		this.mappedChargeLevel = mappedChargeLevel;
	}


	public int getChargeLevelId() {
		return chargeLevelId;
	}


	public void setChargeLevelId(int chargeLevelId) {
		this.chargeLevelId = chargeLevelId;
	}


	public String getLevel() {
		return level;
	}


	public void setLevel(String level) {
		this.level = level;
	}


	public String getChargeLevelDescription() {
		return chargeLevelDescription;
	}


	public void setChargeLevelDescription(String chargeLevelDescription) {
		this.chargeLevelDescription = chargeLevelDescription;
	}

	 
		
	}
