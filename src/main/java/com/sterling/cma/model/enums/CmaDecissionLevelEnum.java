package com.sterling.cma.model.enums;

public enum CmaDecissionLevelEnum {
	LEVEL_1(1, "Level 1"), 
	LEVEL_2(2, "Level 2"), 
	LEVEL_3(3, "Level 3");

	private CmaDecissionLevelEnum(int id, String name) {
		this.id = id;
		this.name = name;
	}

	int id;
	String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
