package com.sterling.cma.model.enums;

public enum ParamEnum {
	CUSTOMER(1, "Customer", "Customer Name", 1, 0),
	ACCOUNT(2, "Account", "Account Name", 2, 0),
	PROFILE(3, "Profile", "Profile", 3, 0),
	PRODUCT_CATEGORY(4, "ProductCategory", "Product Category", 4, 0),
	PARENT_SYSTEM(5, "ParentSystem", "Parent System", 5, 0),
	MODEL_ID(6, "ModelId", "Model Id", 6, 0),
	CMA_TIRE(7, "CmaType", "Cma Type", 7, 0),
	CMA_TYPE(8, "ModelId", "Model Id", 8, 0),
	LEVEL(9, "Level", "Level", 9, 0),
	GROUP_CHARGE_CATEGORY(10, "GroupChargeCategory", "Group Charge Category", 10, 0),
	CHARGE_CATEGORY(11, "ChargeCategory", "Charge Category", 11, 0), 
	EXCEPT_CHARGE_CATEGORY(12, "ExceptChargeCategory", "Except Charge Category", 12, 0),
	GROUP_CHARGE_LEVEL(13, "GroupChargeLevel", "Group Charge Level", 13, 0),
	CHARGE_LEVEL(14, "ChargeLevel", "Charge Level", 14, 0), 	
	DISPOSITION_TYPE(15, "DispositionType", "Disposition Type", 15, 0),
	DISPOSITION(16, "Disposition", "Disposition", 16, 0),
	EXCEPT_DISPOSITION(17, "ExceptDisposition", "Except Disposition", 17, 0),
	FREQUENCY(18, "Frequency", "Frequency", 18, 0),
	FREQUENCY_OPERATOR(19, "FrequencyOperator", "Frequency Operator", 19, 0),
	AGE_OF_RECORD(20, "AgeOfRecord", "Age of Record", 20, 0),
	AGE_OF_RECORD_OPERATOR(21, "AgeOfRecordOperator", "Age Of Record Operator", 21, 0),
	AGE_OF_SENTENCE(22, "AgeOfSentence", "Age of Sentence", 22, 0),
	AGE_OF_SENTENCE_OPERATOR(23, "AgeOfSentenceOperator", "Age of Sentence Operator", 23, 0),
	AGE_OF_SENTENCE_PROBATION(24, "AgeOfSentenceProbation", "Age of Sentence Probation", 24, 0),
	AGE_OF_SENTENCE_PROBATION_OPERATOR(25, "AgeOfSentenceProbationOperator", "Age of Sentence Probation Operator", 25, 0),
	WARRANT_ACTIVE(26, "WarrantActive", "Warrant Active", 26, 0),
	SENTENCE_ACTIVE(27, "SentenceActive", "Sentence Active", 27, 0),
	STATE(28, "State", "State", 28, 0),
	EXCEPT_STATE(29, "ExceptState", "Except State", 29, 0),
	DEFENDANT_AGE(30, "DefendantAge", "Defendant Age", 30, 0),
	PROBATION_STATUS(31, "ProbationStatus", "Probation Status", 31, 0);
	
	int id;
	String name;
	String description;
	int order;
	int weightage;
	
	private ParamEnum(int id, String name, String description, int order, int weightage) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.order = order;
		this.weightage = weightage;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public int getWeightage() {
		return weightage;
	}

	public void setWeightage(int weightage) {
		this.weightage = weightage;
	}
}
