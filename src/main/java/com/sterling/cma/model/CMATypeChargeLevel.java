package com.sterling.cma.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sterling.cma.model.enums.ChargeLevelEnum;

public class CMATypeChargeLevel extends AbstractCMAType {

	private static Logger logger = LoggerFactory.getLogger(CMATypeChargeLevel.class);

	public CMATypeChargeLevel(CMAType inner) {
		super(inner);
		// TODO Auto-generated constructor stub
	}

	public Collection<Predicate<CrimCharge>> score(Collection<Predicate<CrimCharge>> filters,
			Collection<Predicate<CrimCharge>> exceptionfilters,
			Collection<Predicate<CrimCharge>> groupChargelevelFilter, Collection<Predicate<CrimCharge>> dispositionFilter,Collection<Predicate<CrimCharge>> stateFilter,CmaRule cmaRule) {

		super.score(filters, exceptionfilters, groupChargelevelFilter, dispositionFilter, stateFilter,cmaRule);
		doChargeLevel(filters, groupChargelevelFilter, cmaRule);
		return null;

	}

	private void doChargeLevel(Collection<Predicate<CrimCharge>> filters,
			Collection<Predicate<CrimCharge>> groupChargelevelFilter, CmaRule cmaRule) {
		// TODO Auto-generated method stub
		// logger.info("setting chargelevel predicate for "+cmaRule.getChargeLevel());

		/*
		 * Predicate<CrimCharge> dispPredicate = p->{ String dispostion =
		 * p.getChargeLevel(); return dispostion!=null &&
		 * p.getChargeLevel().equalsIgnoreCase(cmaRule.getChargeLevel()); };
		 * filters.add(dispPredicate);
		 */
		if (cmaRule.getChargeLevel() != null) {
			String[] chargeLevels = cmaRule.getChargeLevel().split(",");
			for (String chargeLevel : chargeLevels) {
				String cl = chargeLevel.toUpperCase().trim();
				if (null != cl) {
					String[] exDisp = mapData.get(chargeLevel.toUpperCase().trim()).split(",");
					for (String cat : exDisp) {

						Predicate<CrimCharge> dispPredicate = p -> {
							String dispostion = p.getChargeLevel();
							return dispostion != null && p.getChargeLevel().equalsIgnoreCase(cat.trim());
						};
						groupChargelevelFilter.add(dispPredicate);
					}

				}
			}
		} else {
			Predicate<CrimCharge> dispPredicate = p -> {
				String dispostion = p.getChargeLevel();
				return dispostion != null && p.getChargeLevel().equalsIgnoreCase(cmaRule.getChargeLevel().trim());
			};
			filters.add(dispPredicate);
		}
	}

	private static final Map<String, String> mapData;

	static {
		mapData = new HashMap<String, String>();

		mapData.put("FELONY", "FELONY,INDICTABLE");
		mapData.put("INDICTABLE", "INDICTABLE");

		mapData.put("MISDEMEANOR",
				"MISDEMEANOR,GROSS_MISDEMEANOR,SERIOUS_MISDEMEANOR,SIMPLE_MISDEMEANOR,AGRRAVATED_MISDEMEANOR,NON_INDICTABLE,"
						+ "PETTY_MISDEMEANOR");
		mapData.put("GROSS_MISDEMEANOR", "GROSS_MISDEMEANOR");
		mapData.put("SERIOUS_MISDEMEANOR", "SERIOUS_MISDEMEANOR");
		mapData.put("SIMPLE_MISDEMEANOR", "SIMPLE_MISDEMEANOR");
		mapData.put("AGRRAVATED_MISDEMEANOR", "AGRRAVATED_MISDEMEANOR");
		mapData.put("NON_INDICTABLE", "NON_INDICTABLE");
		mapData.put("PETTY_MISDEMEANOR", "PETTY_MISDEMEANOR");
		mapData.put("VIOLATION",
				"VIOLATION,TRAFFIC,INFRACTION,ORDINANCE_VIOLATION,MUNICIPAL_ORDINANCE,FORFEITURE,PETTY_OFFENSE"
						+ "SUMMARY,SUMMARY_PA,PETTY_MN,MISCELLANEOUS,DISORDERLY_PERSON");
		mapData.put("TRAFFIC", "TRAFFIC");
		mapData.put("INFRACTION", "INFRACTION");
		mapData.put("ORDINANCE_VIOLATION", "ORDINANCE_VIOLATION");
		mapData.put("MUNICIPAL_ORDINANCE", "MUNICIPAL_ORDINANCE");
		mapData.put("FORFEITURE", "FORFEITURE");
		mapData.put("PETTY_OFFENSE", "PETTY_OFFENSE");
		mapData.put("SUMMARY", "SUMMARY");
		mapData.put("SUMMARY_PA", "SUMMARY_PA");
		mapData.put("PETTY_MN", "PETTY_MN");
		mapData.put("MISCELLANEOUS", "MISCELLANEOUS");
		mapData.put("DISORDERLY_PERSON", "DISORDERLY_PERSON");
		mapData.put("UNKNOWN", "UNKNOWN,DATAENTRY,NOT_LISTED,SEALED,SHOW_CAUSE,CAPIAS,BRANCH,UNDESIGNATED");
		mapData.put("DATAENTRY", "DATAENTRY");
		mapData.put("NOT_LISTED", "NOT_LISTED");
		mapData.put("SEALED", "SEALED");
		mapData.put("SHOW_CAUSE", "SHOW_CAUSE");
		mapData.put("CAPIAS", "CAPIAS");
		mapData.put("BRANCH", "BRANCH");
		mapData.put("UNDESIGNATED", "UNDESIGNATED");

	}

}
