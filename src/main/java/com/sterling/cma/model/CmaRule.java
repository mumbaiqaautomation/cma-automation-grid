package com.sterling.cma.model;

import java.util.ArrayList;
import java.util.List;

public class CmaRule {

	private String modelId;
	private String ruleId;
	private Integer frequency;
	private Double ageOfRecord;
	private Double ageOfSentence;
	private String frequencyGroupId;
	private String score;
	private String ruleType;
	private List<String> exceptCategory = new ArrayList<String>();
	private List<String> exceptDispostion = new ArrayList<String>();
	private List<String> exceptState = new ArrayList<String>();
	private Double ageOfDefendant;
	private String ageOfDefendantOperator;
	private String chargeLevel;
	private String dispositionType;
	private String chargeCategory;
	private String warrantStatus;
	private String probationStatus;

	
	private String frequencyOperator;
	
	public String getFrequencyOperator() {
		return frequencyOperator;
	}
	public void setFrequencyOperator(String frequencyOperator) {
		this.frequencyOperator = frequencyOperator;
	}
	public String getAgeOfDefendantOperator() {
		return ageOfDefendantOperator;
	}
	public void setAgeOfDefendantOperator(String ageOfDefendantOperator) {
		this.ageOfDefendantOperator = ageOfDefendantOperator;
	}
	public Double getAgeOfDefendant() {
		return ageOfDefendant;
	}
	public void setAgeOfDefendant(Double ageOfDefendant) {
		this.ageOfDefendant = ageOfDefendant;
	}
	public List<String> getExceptState() {
		return exceptState;
	}
	public void setExceptState(List<String> exceptState) {
		this.exceptState = exceptState;
	}
	public List<String> getExceptDispostion() {
		return exceptDispostion;
	}
	public void setExceptDispostion(List<String> exceptDispostion) {
		this.exceptDispostion = exceptDispostion;
	}
	
	
	
	public void setProbationStatus(String probationStatus) {
		this.probationStatus = probationStatus;
	}
	private String disposition;
	
	private Double ageOfSentenceProbation;
	
	private String sentenceStatus;
	
	public Double getAgeOfSentenceProbation() {
		return ageOfSentenceProbation;
	}
	public void setAgeOfSentenceProbation(Double ageOfSentenceProbation) {
		this.ageOfSentenceProbation = ageOfSentenceProbation;
	}
	public String getSentenceStatus() {
		return sentenceStatus;
	}
	public void setSentenceStatus(String sentenceStatus) {
		this.sentenceStatus = sentenceStatus;
	}
	public String getDisposition() {
		return disposition;
	}
	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}
	//operators
	private String ageOfRecordOperator;
	
	//operators
		private String ageOfSentenceProbationOperator;
	
	public String getAgeOfSentenceProbationOperator() {
			return ageOfSentenceProbationOperator;
		}
		public void setAgeOfSentenceProbationOperator(String ageOfSentenceProbationOperator) {
			this.ageOfSentenceProbationOperator = ageOfSentenceProbationOperator;
		}
		//operators
		private String ageOfSentenceOperator;
	
	public String getAgeOfSentenceOperator() {
			return ageOfSentenceOperator;
		}
		public void setAgeOfSentenceOperator(String ageOfSentenceOperator) {
			this.ageOfSentenceOperator = ageOfSentenceOperator;
		}
	public String getAgeOfRecordOperator() {
		return ageOfRecordOperator;
	}
	public void setAgeOfRecordOperator(String ageOfRecordOperator) {
		this.ageOfRecordOperator = ageOfRecordOperator;
	}
	public String getChargeLevel() {
		return chargeLevel;
	}
	public void setChargeLevel(String chargeLevel) {
		this.chargeLevel = chargeLevel;
	}
	public String getDispositionType() {
		return dispositionType;
	}
	public void setDispositionType(String dispositionType) {
		this.dispositionType = dispositionType;
	}
	public String getChargeCategory() {
		return chargeCategory;
	}
	public void setChargeCategory(String chargeCategory) {
		this.chargeCategory = chargeCategory;
	}
	public String getWarrantStatus() {
		return warrantStatus;
	}
	public void setWarrantStatus(String warrantStatus) {
		this.warrantStatus = warrantStatus;
	}
	public String getCategoryLevel() {
		return categoryLevel;
	}
	public void setCategoryLevel(String categoryLevel) {
		this.categoryLevel = categoryLevel;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	private String categoryLevel;
	private String state;
	
	
	public String getRuleType() {
		return ruleType;
	}
	public void setRuleType(String ruleType) {
		this.ruleType = ruleType;
	}
	public String getScore() {
		return score;
	}
	public void setScore(String score) {
		this.score = score;
	}
	public String getFrequencyGroupId() {
		return frequencyGroupId;
	}
	public void setFrequencyGroupId(String frequencyGroupId) {
		this.frequencyGroupId = frequencyGroupId;
	}
	public String getModelId() {
		return modelId;
	}
	public void setModelId(String modelId) {
		this.modelId = modelId;
	}
	public String getRuleId() {
		return ruleId;
	}
	public void setRuleId(String ruleId) {
		this.ruleId = ruleId;
	}
	public Integer getFrequency() {
		return frequency;
	}
	public void setFrequency(Integer frequency) {
		this.frequency = frequency;
	}
	public Double getAgeOfRecord() {
		return ageOfRecord;
	}
	public void setAgeOfRecord(Double ageOfRecord) {
		this.ageOfRecord = ageOfRecord;
	}
	public Double getAgeOfSentence() {
		return ageOfSentence;
	}
	public void setAgeOfSentence(Double ageOfSentence) {
		this.ageOfSentence = ageOfSentence;
	}
	/**
	 * @return the exceptCategory
	 */
	public List<String> getExceptCategory() {
		return exceptCategory;
	}
	/**
	 * @param exceptCategory the exceptCategory to set
	 */
	public void setExceptCategory(List<String> exceptCategory) {
		this.exceptCategory = exceptCategory;
	}
	public String getProbationStatus() {
		// TODO Auto-generated method stub
		return this.probationStatus;
	}
}