package com.sterling.cma.model;

import java.util.Set;

public class ParamGroup {
	private Long paramGroupId;
	private String paramGroupName;
	private String paramGroupLogicalName;
	private String paramGroupDescription;

	private Set<Param> paramGroups;

	public Long getParamGroupId() {
		return paramGroupId;
	}

	public void setParamGroupId(Long paramGroupId) {
		this.paramGroupId = paramGroupId;
	}

	public String getParamGroupName() {
		return paramGroupName;
	}

	public void setParamGroupName(String paramGroupName) {
		this.paramGroupName = paramGroupName;
	}	

	public String getParamGroupLogicalName() {
		return paramGroupLogicalName;
	}

	public void setParamGroupLogicalName(String paramGroupLogicalName) {
		this.paramGroupLogicalName = paramGroupLogicalName;
	}

	public String getParamGroupDescription() {
		return paramGroupDescription;
	}

	public void setParamGroupDescription(String paramGroupDescription) {
		this.paramGroupDescription = paramGroupDescription;
	}

	public Set<Param> getParamGroups() {
		return paramGroups;
	}

	public void setParamGroups(Set<Param> paramGroups) {
		this.paramGroups = paramGroups;
	}
}
