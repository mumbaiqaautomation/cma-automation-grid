package com.sterling.cma.model;

public class ParentSystem {
	private Integer parentSystemId;
	private String parentSystem;
	private String description;

	/**
	 * @return the parentSystemId
	 */
	public Integer getParentSystemId() {
		return parentSystemId;
	}

	/**
	 * @param parentSystemId
	 *            the parentSystemId to set
	 */
	public void setParentSystemId(final Integer parentSystemId) {
		this.parentSystemId = parentSystemId;
	}

	/**
	 * @return the parentSystem
	 */
	public String getParentSystem() {
		return parentSystem;
	}

	/**
	 * @param parentSystem
	 *            the parentSystem to set
	 */
	public void setParentSystem(final String parentSystem) {
		this.parentSystem = parentSystem;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "ParentSystem [parentSystemId=" + parentSystemId + ", parentSystem=" + parentSystem + ", description="
				+ description + "]";
	}
}
