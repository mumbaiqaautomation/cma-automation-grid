package com.sterling.cma.model;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class CrimCase implements Cloneable{
	private Long crimCaseId;
	private Set<CrimCharge> charges = new HashSet<CrimCharge>();
//	case unique number
	private String number;

	public Long getCrimCaseId() {
		return crimCaseId;
	}

	public void setCrimCaseId(Long crimCaseId) {
		this.crimCaseId = crimCaseId;
	}

	public Set<CrimCharge> getCharges() {
		return charges;
	}

	public void setCharges(Set<CrimCharge> charges) {
		this.charges = charges;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@Override
	public String toString() {
		return "CrimCase [crimCaseId=" + crimCaseId + ", charges=" + charges + ", number=" + number + "]";
	}

	public CrimCase(CrimCase crimCase) {
		crimCase.charges = this.charges.stream().map(CrimCharge::new).collect(Collectors.toSet());
		crimCase.crimCaseId = this.crimCaseId;
		crimCase.number = this.number;
		
		
		
	}
	
	public CrimCase() {
		
		
		
		
	}
}
