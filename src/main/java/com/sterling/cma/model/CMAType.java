package com.sterling.cma.model;


import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

public abstract class CMAType {

	
	
	
	public List<String> getAllCMATypes(){
		
		return null;
		
	}

	public abstract Collection<Predicate<CrimCharge>> score(Collection<Predicate<CrimCharge>> filters,
			Collection<Predicate<CrimCharge>> exceptionfilters,
			Collection<Predicate<CrimCharge>> groupChargelevelFilter, Collection<Predicate<CrimCharge>> dispositionFilter, Collection<Predicate<CrimCharge>> stateFilter, CmaRule cmaRule) ;
	


}
