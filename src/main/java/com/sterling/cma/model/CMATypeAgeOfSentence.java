package com.sterling.cma.model;


import java.util.Collection;
import java.util.function.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sterling.cma.model.enums.ParamOperatorEnum;

public class CMATypeAgeOfSentence extends AbstractCMAType{
	
	private static Logger logger = LoggerFactory.getLogger(CMATypeAgeOfSentence.class);

	public CMATypeAgeOfSentence(CMAType inner) {
		super(inner);
		// TODO Auto-generated constructor stub
	}
	
	public Collection<Predicate<CrimCharge>>  score(Collection<Predicate<CrimCharge>> filters,Collection<Predicate<CrimCharge>> exceptionfilters,
			Collection<Predicate<CrimCharge>> groupChargelevelFilter,Collection<Predicate<CrimCharge>> dispositionFilter,Collection<Predicate<CrimCharge>> stateFilter,
			CmaRule cmaRule) {
		super.score(filters,exceptionfilters,groupChargelevelFilter,dispositionFilter,stateFilter,cmaRule);
		doAgeOfSentence(filters,cmaRule);
		return null;
		
	}

	private void doAgeOfSentence(Collection<Predicate<CrimCharge>> filters,CmaRule cmaRule) {
		
		//logger.info("setting filter for AgeofSentence" + cmaRule.getAgeOfSentence() + " op" + cmaRule.getAgeOfSentenceOperator());
		setOperator(cmaRule.getAgeOfSentenceOperator());
		
		Predicate<CrimCharge> catPredicate;
		switch (paramOperatorEnum){
		
		case GTHAN:
			
			 catPredicate = p -> 
		     Double.parseDouble(p.getAgeOfSentence())  > cmaRule.getAgeOfSentence().doubleValue();//n
		     break;
		case GETHAN:
			
			catPredicate = p -> 
			Double.parseDouble(p.getAgeOfSentence())  >= cmaRule.getAgeOfSentence().doubleValue();
		     break;
		case LTHAN:
			
			 catPredicate = p -> 
			 Double.parseDouble(p.getAgeOfSentence())  < cmaRule.getAgeOfSentence().doubleValue();//n
		     break;
		case LETHAN:
			
			catPredicate = p -> 
			Double.parseDouble(p.getAgeOfSentence())  <= cmaRule.getAgeOfSentence().doubleValue();
		     break;		     
		case EQ:
			
			catPredicate = p -> 
			Double.parseDouble(p.getAgeOfSentence())  == cmaRule.getAgeOfSentence().doubleValue();
		     break;		     

		default:
			catPredicate = p-> Double.parseDouble(p.getAgeOfSentence())  >= 0;
		}
		filters.add(catPredicate);
		
	}

}
