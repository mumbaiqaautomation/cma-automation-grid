package com.sterling.cma.model;

import java.util.HashSet;
import java.util.Set;

public class CmaRequest {
	private Long cmaRequestId;
	private Set<CrimCase> cases = new HashSet<CrimCase>();
	private String modelId;
	private String number;
	private String cmaTrackingId;

	public String getCmaTrackingId() {
		return cmaTrackingId;
	}

	public void setCmaTrackingId(String cmaTrackingId) {
		this.cmaTrackingId = cmaTrackingId;
	}

	public Long getCmaRequestId() {
		return cmaRequestId;
	}

	public void setCmaRequestId(Long cmaRequestId) {
		this.cmaRequestId = cmaRequestId;
	}

	public Set<CrimCase> getCases() {
		return cases;
	}

	public void setCases(Set<CrimCase> cases) {
		this.cases = cases;
	}

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	@Override
	public String toString() {
		return "CmaRequest [cmaRequestId=" + cmaRequestId + ", cases=" + cases + ", modelId=" + modelId + "]";
	}
}
