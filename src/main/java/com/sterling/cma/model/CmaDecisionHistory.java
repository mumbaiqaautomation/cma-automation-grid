package com.sterling.cma.model;

import java.util.Date;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAutoGenerateStrategy;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAutoGeneratedKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAutoGeneratedTimestamp;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName="to-be-picked-from-propertyfile")
public class CmaDecisionHistory {
	
	
	
	    private String cmaresponseid;
	    private String payload;
	    private String rules;
	    private String cmarequestid;
	    private Date createdDate;
	    
	  
	    @DynamoDBHashKey(attributeName = "cma-response-id")
	    @DynamoDBAutoGeneratedKey
	    public String getCmaresponseid() {
			return cmaresponseid;
		}
		public void setCmaresponseid(String cmaresponseid) {
			this.cmaresponseid = cmaresponseid;
		}
		
		@DynamoDBAttribute(attributeName="cmarequestid")
		public String getCmarequestid() {
			return cmarequestid;
		}
		public void setCmarequestid(String cmarequestid) {
			this.cmarequestid = cmarequestid;
		}
		@DynamoDBAttribute(attributeName="rules")
		public String getRules() {
			return rules;
		}
		public void setRules(String rules) {
			this.rules = rules;
		}
		
	    
	    @DynamoDBAttribute(attributeName="payload")
	    public String getPayload() { return this.payload; }
	    public void setPayload(String payload) { this.payload = payload; }    
	   
	    
	    @DynamoDBAutoGeneratedTimestamp(strategy=DynamoDBAutoGenerateStrategy.CREATE)
	    public Date getCreatedDate() { return createdDate; }
	    public void setCreatedDate(Date createdDate) { this.createdDate = createdDate; }

}
