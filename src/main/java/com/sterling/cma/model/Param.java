package com.sterling.cma.model;

import com.sterling.cma.model.enums.ParamDataTypeEnum;

public class Param {
	private Long paramId;
	private String paramName;
	private String paramLogicalName;
	private String paramDescription;
	private ParamDataTypeEnum paramDataType;
	
	private ParamGroup paramGroup;

	public Long getParamId() {
		return paramId;
	}

	public void setParamId(Long paramId) {
		this.paramId = paramId;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getParamLogicalName() {
		return paramLogicalName;
	}

	public void setParamLogicalName(String paramLogicalName) {
		this.paramLogicalName = paramLogicalName;
	}

	public String getParamDescription() {
		return paramDescription;
	}

	public void setParamDescription(String paramDescription) {
		this.paramDescription = paramDescription;
	}

	public ParamDataTypeEnum getParamDataType() {
		return paramDataType;
	}

	public void setParamDataType(ParamDataTypeEnum paramDataType) {
		this.paramDataType = paramDataType;
	}

	public ParamGroup getParamGroup() {
		return paramGroup;
	}

	public void setParamGroup(ParamGroup paramGroup) {
		this.paramGroup = paramGroup;
	}
}
