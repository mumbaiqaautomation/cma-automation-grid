package com.sterling.cma.model;

public class CrimSentence {
	private Long crimSentenceId;
//	not sure about the use of this param.
	private String sentencyType;

	public Long getCrimSentenceId() {
		return crimSentenceId;
	}

	public void setCrimSentenceId(Long crimSentenceId) {
		this.crimSentenceId = crimSentenceId;
	}

	public String getSentencyType() {
		return sentencyType;
	}

	public void setSentencyType(String sentencyType) {
		this.sentencyType = sentencyType;
	}

}
