package com.sterling.cma.model;

import java.util.Collection;

import java.util.function.Predicate;

public class CMATypeSentenceActive extends AbstractCMAType{

	public CMATypeSentenceActive(CMAType inner) {
		super(inner);
		// TODO Auto-generated constructor stub
	}
	
	public Collection<Predicate<CrimCharge>>  score(Collection<Predicate<CrimCharge>> filters,Collection<Predicate<CrimCharge>> exceptionfilters,
			Collection<Predicate<CrimCharge>> groupChargelevelFilter,Collection<Predicate<CrimCharge>> dispositionFilter,Collection<Predicate<CrimCharge>> stateFilter,CmaRule cmaRule) {
		super.score(filters,exceptionfilters,groupChargelevelFilter,dispositionFilter,stateFilter,cmaRule);
		doSentenceActive(filters,cmaRule);
		return null;
		
	}

	private void doSentenceActive(Collection<Predicate<CrimCharge>> filters,CmaRule cmaRule) {
		
		// TODO Auto-generated method stub
		//filters.add(e)
		Predicate<CrimCharge> dispPredicate = p->{
			String dispositionPredicate = p.getSentenceStatus();
			return dispositionPredicate!= null && p.getSentenceStatus().equalsIgnoreCase(cmaRule.getSentenceStatus());
		};
		filters.add(dispPredicate);
		
	}

}
