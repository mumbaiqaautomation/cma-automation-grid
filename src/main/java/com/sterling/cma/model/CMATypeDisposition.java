package com.sterling.cma.model;

import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

public class CMATypeDisposition extends AbstractCMAType{

	public CMATypeDisposition(CMAType inner) {
		super(inner);
		// TODO Auto-generated constructor stub
	}
	
	public Collection<Predicate<CrimCharge>>  score(Collection<Predicate<CrimCharge>> filters,Collection<Predicate<CrimCharge>> exceptionfilters,
			Collection<Predicate<CrimCharge>> groupChargelevelFilter,Collection<Predicate<CrimCharge>> dispositionFilter,Collection<Predicate<CrimCharge>> stateFilter,CmaRule cmaRule) {
		super.score(filters,exceptionfilters,groupChargelevelFilter,dispositionFilter, stateFilter,cmaRule);
		doDispostion(filters,exceptionfilters,cmaRule);
		return null;
		
	}

	private void doDispostion(Collection<Predicate<CrimCharge>> filters,Collection<Predicate<CrimCharge>> exceptionfilters,CmaRule cmaRule) {
		
		// TODO Auto-generated method stub
		//filters.add(e)
		/*Predicate<CrimCharge> dispPredicate = p->{
			String dispositionPredicate = p.getDisposition();
			return dispositionPredicate!= null && p.getDisposition().equalsIgnoreCase(cmaRule.getDisposition());
		};
		filters.add(dispPredicate);*/
		//////////////////////////////////////////////////
		
		if (cmaRule.getExceptDispostion() == null || cmaRule.getExceptDispostion().isEmpty()) {
			
			Predicate<CrimCharge> dispPredicate = p->{
				String dispositionPredicate = p.getDisposition();
				return dispositionPredicate!= null && p.getDisposition().equalsIgnoreCase(cmaRule.getDisposition());
			};
			filters.add(dispPredicate);
			
		} else {
			List<String> exDisp = cmaRule.getExceptDispostion();
			for (String cat : exDisp) {

				Predicate<CrimCharge> dispPredicate = p -> p.getDisposition().equalsIgnoreCase(cat);
				exceptionfilters.add(dispPredicate);

			}

		}
		
	}

}
