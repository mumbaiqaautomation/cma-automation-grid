package com.sterling.cma.model;

public class CustomerRuleConfiguration {
	private Long customerRuleConfigurationId;
	private CustomerMasterSetting customerMasterSetting;
	private ParamGroup paramGroup;

	public Long getCustomerRuleConfigurationId() {
		return customerRuleConfigurationId;
	}

	public void setCustomerRuleConfigurationId(Long customerRuleConfigurationId) {
		this.customerRuleConfigurationId = customerRuleConfigurationId;
	}

	public CustomerMasterSetting getCustomerMasterSetting() {
		return customerMasterSetting;
	}

	public void setCustomerMasterSetting(CustomerMasterSetting customerMasterSetting) {
		this.customerMasterSetting = customerMasterSetting;
	}

	public ParamGroup getParamGroup() {
		return paramGroup;
	}

	public void setParamGroup(ParamGroup paramGroup) {
		this.paramGroup = paramGroup;
	}
}
