package com.sterling.cma.model;

public class CustomerMasterSetting {
	private Long customerSettingId;
	private Customer customer;
	private Account account;
	private Profile profile;

	public Long getCustomerSettingId() {
		return customerSettingId;
	}

	public void setCustomerSettingId(Long customerSettingId) {
		this.customerSettingId = customerSettingId;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}
}
