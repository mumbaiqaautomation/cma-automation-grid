package com.sterling.cma.model.constant;

import java.util.ArrayList;

public interface CmaConstant {
	public interface ExcellConstant {
		public static final int COL_INDEX_CUSTOMER = 0;
		public static final int COL_INDEX_ACCOUNT = 1;
		public static final int COL_INDEX_PROFILE = 2;
		public static final int COL_INDEX_PRODUCT_CATEGORY = 3;
		public static final int COL_INDEX_PARENT_SYSTEM = 4;
		public static final int COL_INDEX_TIRE = 5;
		public static final int COL_INDEX_CMA_TIRE = 6;
		public static final int COL_INDEX_CMA_TYPE = 7;
		public static final int COL_INDEX_LEVEL = 8;
		public static final int COL_INDEX_GROUP_CHARGE_CATEGORY = 9;
		//public static final int COL_INDEX_CHARGE_CATEGORY = 10;
		public static final int COL_INDEX_LEVEL1_CHARGE_CATEGORY = 10;
		public static final int COL_INDEX_LEVEL2_CHARGE_CATEGORY = 11;
		public static final int COL_INDEX_LEVEL3_CHARGE_CATEGORY = 12;
		//public static final int COL_INDEX_EXCEPT_CATEGORY = 11;
		public static final int COL_INDEX_LEVEL1_EXCEPT_CATEGORY = 13;
		public static final int COL_INDEX_LEVEL2_EXCEPT_CATEGORY = 14;
		public static final int COL_INDEX_LEVEL3_EXCEPT_CATEGORY = 15;
		public static final int COL_INDEX_GROUP_CHARGE_LEVEL = 16;
		public static final int COL_INDEX_CHARGE_LEVEL = 17;
		public static final int COL_INDEX_DISPOSITION_TYPE = 18;
		public static final int COL_INDEX_DISPOSITION = 19;
		public static final int COL_INDEX_EXCEPT_DISPOSITION = 20;
		public static final int COL_INDEX_FREQUENCY = 21;
		public static final int COL_INDEX_FREQUENCY_OPERATOR = 22;
		public static final int COL_INDEX_AGE_OF_RECORD = 23;
		public static final int COL_INDEX_AGE_OF_RECORD_OPERATOR = 24;
		public static final int COL_INDEX_AGE_OF_SENTENCE = 25;
		public static final int COL_INDEX_AGE_OF_SENTENCE_OPERATOR = 26;
		public static final int COL_INDEX_AGE_OF_SENTENCE_PROBATION = 27;
		public static final int COL_INDEX_AGE_OF_SENTENCE_PROBATION_OPERATOR = 28;
		public static final int COL_INDEX_WARRANT_ACTIVE = 29;
		public static final int COL_INDEX_SENTENCE_ACTIVE = 30;
		public static final int COL_INDEX_STATE = 31;
		public static final int COL_INDEX_EXCEPT_STATE = 32;
		public static final int COL_INDEX_DEFENDANT_AGE = 33;
		public static final int COL_INDEX_PROBATION_STATUS = 34;
		public static final int COL_INDEX_MODELID = 35;
		public static final int COL_INDEX_EXPECTED_LEVEL = 36;
		public static final int COL_INDEX_EXPECTED_CMATYPE = 37;
		public static final int COL_INDEX_ACTUAL_LEVEL = 38;
		public static final int COL_INDEX_STATUS = 39;
	}

	public interface CmaSettingColumnName {
		public static final String MODEL_ID = "model_id";
		public static final String RULE_ID = "rule_id";
		public static final String CMA_TIER = "cma_tier";
		public static final String CMA_TYPE = "cma_type";
		public static final String CMA_SCORE = "cma_score";
		public static final String CHARGE_CATEGORY = "charge_category";
		public static final String CHARGE_LEVEL = "charge_level";
		public static final String DISPOSITION_TYPE = "disposition_type";
		public static final String DISPOSITION = "disposition";
		public static final String FREQUENCY = "frequency";
		public static final String FREQUENCY_OPERATOR = "frequency_operator";
		public static final String AGE_OF_RECORD = "age_of_record";
		public static final String AGE_OF_RECORD_OPERATOR = "age_of_record_operator";
		public static final String AGE_OF_SENTENCE = "age_of_sentence";
		public static final String AGE_OF_SENTENCE_OPERATOR = "age_of_sentence_operator";
		public static final String AGE_OF_SENTENCE_PROBATION = "age_of_sentence_probation";
		public static final String AGE_OF_SENTENCE_PROBATION_OPERATOR = "age_of_sentence_probation_operator";
		public static final String AGE_OF_DEFENDANT_OPERATOR = "age_of_defendant_operator";
		public static final String WARRANT_ACTIVE = "warrant_active";
		public static final String SENTENCE_ACTIVE = "sentence_active";
		public static final String STATE = "state";
		public static final String DEFENDANT_AGE = "defendant_age";
		public static final String PROBATION_STATUS = "probation_status";
		public static final String CREATION_TIME = "creation_time";
		public static final String EXCEPT_STATE = "except_state";
		public static final String EXCEPT_CHARGE_CATEGORY = "except_charge_category";
		public static final String EXCEPT_DISPOSTION = "except_dispostion";
	}

	public interface CmaTypeConst {
		public static final String CHARGE_CATEGORY = "charge_category";
		public static final String CHARGE_LEVEL = "Charge Level";
		public static final String DISPOSITION_TYPE = "Disposition Type";
		public static final String DISPOSITION = "Disposition";
		public static final String AGE_OF_RECORD = "Age of Record";
		public static final String AGE_OF_SENTENCE = "Age of Sentence";
		public static final String AGE_OF_SENTENCE_PROBATION = "Probation Sentence Age";
		public static final String STATE = "State";
		public static final String EXCEPT_STATE = "Except State";
		public static final String EXCEPT_CHARGE_CATEGORY = "Except ChargeCategory";
		public static final String EXCEPT_DISPOSTION = "Except Disposition";
		public static final String WARRANT = "Warrant";
		public static final String PROBATION_STATUS = "Probation Status";
		public static final String SENTENCE_STATUS = "Sentence Status";
	}

	public interface CacheConstant {
		public static final String CACHE_NAME_CATEGORY = "Category";
		public static final String CACHE_NAME_DISPOSITION = "Disposition";
		public static final String CACHE_NAME_DISPOSITION_TYPE = "DispositionType";
		public static final String CACHE_NAME_CHARGELEVEL = "ChargeLevel";
		public static final String CACHE_NAME_WARRANT = "Warrant";
		public static final String CACHE_NAME_STATE = "State";
		public static final String CACHE_GROUP_CHARGE_CATEGORY = "GroupChargeCategory";
		public static final String CACHE_GROUP_CHARGE_LEVEL = "GroupChargeLevel";
		public static final String CACHE_NAME_EXCEPT_CATEGORY = "ExceptCategory";
		public static final String CACHE_NAME_EXCEPT_DISPOSITION = "ExceptDisposition";
	}

	public interface AutopmationStatus {
		public static final String AUTOMATION_STATUS_SUCCESS = "Success";
		public static final String AUTOMATION_STATUS__FAILURE = "Failure";
	}

	public interface ParameterStatus {
		public static final ArrayList<String> STATUS_LIST = new ArrayList<String>() {
			{
				add("true");
				add("false");
			}
		};
	}

	public interface CmaSettingLogicalOperation {
		public static final int AND = 1;
		public static final int OR = 0;
		public static final int NOT = 2;
	}

	public interface CmaConstCommon {
		public static final String RULE_SETTING_LOGICAL_SEPERATOR = "-";
		public static final String RULE_SETTING_LOGICAL_DEFAULT_VALUE = "XXX";
		public static final String EXCEPT_STATE_SEPERATOR = ",";
		public static final String EXCEPT_GROUP_CHARGE_CATEGORY_SEPERATOR = ",";
	}

	public interface CmaArithmeticOperator {
		public static final ArrayList<String> ARITHMETIC_OPERATOR_LIST = new ArrayList<String>() {
			{
				add("eq");
				add("gt");
				add("lt");
				add("gteq");
				add("lteq");
				add("not");
			}
		};
		public static final String ARITHMETIC_OPERATOR_DEFAULT = "lteq";
		public static final String ARITHMETIC_OPERATOR_EQ = "eq";
		public static final String ARITHMETIC_OPERATOR_GT = "gt";
		public static final String ARITHMETIC_OPERATOR_LT = "lt";
		public static final String ARITHMETIC_OPERATOR_GT_EQ = "gteq";
		public static final String ARITHMETIC_OPERATOR_LN_EQ = "lteq";
		public static final String ARITHMETIC_OPERATOR_NOT = "not";
	}
}
