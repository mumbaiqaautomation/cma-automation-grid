package com.sterling.cma.model;

public class ParamGroupMaster {
	private Long paramGroupMasterId;
	private Param param;
	private ParamGroup paramGroup;

	public Long getParamGroupMasterId() {
		return paramGroupMasterId;
	}

	public void setParamGroupMasterId(Long paramGroupMasterId) {
		this.paramGroupMasterId = paramGroupMasterId;
	}

	public Param getParam() {
		return param;
	}

	public void setParam(Param param) {
		this.param = param;
	}

	public ParamGroup getParamGroup() {
		return paramGroup;
	}

	public void setParamGroup(ParamGroup paramGroup) {
		this.paramGroup = paramGroup;
	}
}
