package com.sterling.cma.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CmaResponse {
	private Long cmaResponseId;
	private String cmaType;
	
	
	public String getCmaType() {
		return cmaType;
	}

	public void setCmaType(String cmaType) {
		this.cmaType = cmaType;
	}

	private String score;
	private Set<CrimCase> crimCases = new HashSet<CrimCase>();

	public Set<CrimCase> getCrimCases() {
		return crimCases;
	}

	public void setCrimCases(Set<CrimCase> crimCases) {
		this.crimCases = crimCases;
	}

	public Long getCmaResponseId() {
		return cmaResponseId;
	}

	public void setCmaResponseId(Long cmaResponseId) {
		this.cmaResponseId = cmaResponseId;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}
}
