package com.sterling.cma.model;

import java.util.Collection;
import java.util.function.Predicate;

import com.sterling.cma.model.enums.ParamOperatorEnum;

public class AbstractCMAType extends CMAType{
	
	private CMAType cmaType;
	
	ParamOperatorEnum paramOperatorEnum;
	
	public AbstractCMAType(CMAType inner) {
		cmaType = inner;
	}

	@Override
	public Collection<Predicate<CrimCharge>>  score(Collection<Predicate<CrimCharge>> filters,Collection<Predicate<CrimCharge>> exceptionfilters,
			Collection<Predicate<CrimCharge>> groupChargelevelFilter,Collection<Predicate<CrimCharge>> dispositionFilter,Collection<Predicate<CrimCharge>> stateFilter,CmaRule cmaRule) {
		// TODO Auto-generated method stub
	//	System.out.println("AbstractCMAType");
		cmaType.score(filters,exceptionfilters,groupChargelevelFilter,dispositionFilter,stateFilter,cmaRule);
		return null;
	}
	
	public void setOperator(String operator){
		
		if(operator!=null && operator.equalsIgnoreCase("gt")){
			
			paramOperatorEnum = ParamOperatorEnum.GTHAN;
		}
		if(operator!=null && operator.equalsIgnoreCase("gteq")){
			
			paramOperatorEnum = ParamOperatorEnum.GETHAN;
		}
		if(operator!=null && operator.equalsIgnoreCase("lteq")){
			
			paramOperatorEnum = ParamOperatorEnum.LETHAN;
		}
		if(operator!=null && operator.equalsIgnoreCase("lt")){
			
			paramOperatorEnum = ParamOperatorEnum.LTHAN;
		}
		if(operator!=null && operator.equalsIgnoreCase("eq")){
			
			paramOperatorEnum = ParamOperatorEnum.EQ;
		}
		
	}

	
	

}
