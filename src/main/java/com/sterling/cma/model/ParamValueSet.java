package com.sterling.cma.model;

public class ParamValueSet {
	private Long paramValueSetId;

	public Long getParamValueSetId() {
		return paramValueSetId;
	}

	public void setParamValueSetId(Long paramValueSetId) {
		this.paramValueSetId = paramValueSetId;
	}
	
}
