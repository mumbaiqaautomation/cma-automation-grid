package com.sterling.cma.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;



public class CrimCharge {
	private Long crimChargeId;
	// as per Steven's model start
	private String chargeLevel;
	private String dispositionType;
	private String chargeCategory;
	private String warrantStatus;
	private String ageOfRecord;
	private String categoryLevel;
	private String state;
	// not require as of now.
	private String category;
	private String disposition;
	// as per Steven's model end
	// as per Gaurav's requirement start
	private String ageOfSentence;
	
	//@Expose(serialize = false)
	private String ageofSentenceProbation;
	
	
	public String getAgeofSentenceProbation() {
		return ageofSentenceProbation;
	}

	public void setAgeofSentenceProbation(String ageofSentenceProbation) {
		this.ageofSentenceProbation = ageofSentenceProbation;
	}
	private String sentenceStatus;
	private String probationStatus;
	private Set<CrimSentence> crimSentences = new HashSet<CrimSentence>();
	// charge unique number
	private String number;
	private String defendantAge;
	
	private String score;
	private List<String> ruleSetList = new ArrayList<String>();;
	private String cmaType;
//	private String ageofSentenceProbation;	
	
//	public String getAgeofSentenceProbation() {
//		return ageofSentenceProbation;
//	}
//
//	public void setAgeofSentenceProbation(String ageofSentenceProbation) {
//		this.ageofSentenceProbation = ageofSentenceProbation;
//	}

	public String getCmaType() {
		return cmaType;
	}

	public void setCmaType(String cmaType) {
		this.cmaType = cmaType;
	}
	public List<String> getRuleSetList() {
		return ruleSetList;
	}

	public void setRuleSetList(List<String> ruleSetList) {
		this.ruleSetList = ruleSetList;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public Long getCrimChargeId() {
		return crimChargeId;
	}

	public void setCrimChargeId(Long crimChargeId) {
		this.crimChargeId = crimChargeId;
	}

	public String getChargeLevel() {
		return chargeLevel;
	}

	public void setChargeLevel(String chargeLevel) {
		this.chargeLevel = chargeLevel;
	}

	public String getDispositionType() {
		return dispositionType;
	}

	public void setDispositionType(String dispositionType) {
		this.dispositionType = dispositionType;
	}

	public String getChargeCategory() {
		return chargeCategory;
	}

	public void setChargeCategory(String chargeCategory) {
		this.chargeCategory = chargeCategory;
	}

	public String getWarrantStatus() {
		return warrantStatus;
	}

	public void setWarrantStatus(String warrantStatus) {
		this.warrantStatus = warrantStatus;
	}

	public String getAgeOfRecord() {
		return ageOfRecord;
	}

	public void setAgeOfRecord(String ageOfRecord) {
		this.ageOfRecord = ageOfRecord;
	}

	public String getCategoryLevel() {
		return categoryLevel;
	}

	public void setCategoryLevel(String categoryLevel) {
		this.categoryLevel = categoryLevel;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDisposition() {
		return disposition;
	}

	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}

	public String getAgeOfSentence() {
		return ageOfSentence;
	}

	public void setAgeOfSentence(String ageOfSentence) {
		this.ageOfSentence = ageOfSentence;
	}

	public String getSentenceStatus() {
		return sentenceStatus;
	}

	public void setSentenceStatus(String sentenceStatus) {
		this.sentenceStatus = sentenceStatus;
	}

	public String getProbationStatus() {
		return probationStatus;
	}

	public void setProbationStatus(String probationStatus) {
		this.probationStatus = probationStatus;
	}

	public Set<CrimSentence> getCrimSentences() {
		return crimSentences;
	}

	public void setCrimSentences(Set<CrimSentence> crimSentences) {
		this.crimSentences = crimSentences;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getDefendantAge() {
		return defendantAge;
	}

	public void setDefendantAge(String defendantAge) {
		this.defendantAge = defendantAge;
	}

	@Override
	public String toString() {
		return "CrimCharge [crimChargeId=" + crimChargeId + ", chargeLevel=" + chargeLevel + ", dispositionType="
				+ dispositionType + ", chargeCategory=" + chargeCategory + ", warrantStatus=" + warrantStatus
				+ ", ageOfRecord=" + ageOfRecord + ", categoryLevel=" + categoryLevel + ", state=" + state
				+ ", category=" + category + ", disposition=" + disposition + ", ageOfSentence=" + ageOfSentence
				+ ", sentenceStatus=" + sentenceStatus + ", probationStatus=" + probationStatus + ", crimSentences="
				+ crimSentences + ", number=" + number + ", defendantAge=" + defendantAge + "]";
	}	
	
	public CrimCharge(CrimCharge crimCharge) {
		crimCharge.ageOfRecord = this.ageOfRecord;
		crimCharge.ageOfSentence = this.ageOfSentence;
		crimCharge.category = this.category;
		crimCharge.categoryLevel= this.categoryLevel;
		crimCharge.chargeCategory = this.chargeCategory;
		crimCharge.cmaType= this.cmaType;
		crimCharge.crimChargeId=this.crimChargeId;
		crimCharge.number= this.number;
	}
	public CrimCharge() {
		
	}
	
}