package com.sterling.cma.model;

import com.sterling.cma.model.enums.CmaDecissionLevelEnum;
import com.sterling.cma.model.enums.LogicalGroupOperatorEnum;

public class CustomerLogicalOperationGroup {
	private Long customerLogicalOperationGroupId;
	private String logicalOperationGroupName;
	private LogicalGroupOperatorEnum logicalGroupOperatorEnum;
	private CmaDecissionLevelEnum cmaDecissionLevelEnum;

	public Long getCustomerLogicalOperationGroupId() {
		return customerLogicalOperationGroupId;
	}

	public void setCustomerLogicalOperationGroupId(Long customerLogicalOperationGroupId) {
		this.customerLogicalOperationGroupId = customerLogicalOperationGroupId;
	}

	public String getLogicalOperationGroupName() {
		return logicalOperationGroupName;
	}

	public void setLogicalOperationGroupName(String logicalOperationGroupName) {
		this.logicalOperationGroupName = logicalOperationGroupName;
	}

	public LogicalGroupOperatorEnum getLogicalGroupOperatorEnum() {
		return logicalGroupOperatorEnum;
	}

	public void setLogicalGroupOperatorEnum(LogicalGroupOperatorEnum logicalGroupOperatorEnum) {
		this.logicalGroupOperatorEnum = logicalGroupOperatorEnum;
	}

	public CmaDecissionLevelEnum getCmaDecissionLevelEnum() {
		return cmaDecissionLevelEnum;
	}

	public void setCmaDecissionLevelEnum(CmaDecissionLevelEnum cmaDecissionLevelEnum) {
		this.cmaDecissionLevelEnum = cmaDecissionLevelEnum;
	}
}
