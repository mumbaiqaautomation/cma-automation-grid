package com.sterling.cma.model;

public class CmaRequestParamHistory {
	private Long cmaRequestParamHistoryId;
	private CmaRequestLogicalGroupResult cmaRequestLogicalGroupResult;
	private String paramName;
	private Boolean result;

	public Long getCmaRequestParamHistoryId() {
		return cmaRequestParamHistoryId;
	}

	public void setCmaRequestParamHistoryId(Long cmaRequestParamHistoryId) {
		this.cmaRequestParamHistoryId = cmaRequestParamHistoryId;
	}

	public CmaRequestLogicalGroupResult getCmaRequestLogicalGroupResult() {
		return cmaRequestLogicalGroupResult;
	}

	public void setCmaRequestLogicalGroupResult(CmaRequestLogicalGroupResult cmaRequestLogicalGroupResult) {
		this.cmaRequestLogicalGroupResult = cmaRequestLogicalGroupResult;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public Boolean getResult() {
		return result;
	}

	public void setResult(Boolean result) {
		this.result = result;
	}
}
