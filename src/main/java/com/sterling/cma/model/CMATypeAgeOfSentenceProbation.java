package com.sterling.cma.model;


import java.util.Collection;
import java.util.function.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sterling.cma.model.enums.ParamOperatorEnum;

public class CMATypeAgeOfSentenceProbation extends AbstractCMAType{
	
	private static Logger logger = LoggerFactory.getLogger(CMATypeAgeOfSentenceProbation.class);

	public CMATypeAgeOfSentenceProbation(CMAType inner) {
		super(inner);
		// TODO Auto-generated constructor stub
	}
	
	public Collection<Predicate<CrimCharge>>  score(Collection<Predicate<CrimCharge>> filters,Collection<Predicate<CrimCharge>> exceptionfilters,
			Collection<Predicate<CrimCharge>> groupChargelevelFilter,Collection<Predicate<CrimCharge>> dispositionFilter,Collection<Predicate<CrimCharge>> stateFilter,CmaRule cmaRule) {
		super.score(filters,exceptionfilters,groupChargelevelFilter,dispositionFilter,stateFilter,cmaRule);
		doAgeOfSentenceProbation(filters,cmaRule);
		return null;
		
	}

	private void doAgeOfSentenceProbation(Collection<Predicate<CrimCharge>> filters,CmaRule cmaRule) {
		
		//logger.info("setting filter for AgeofSentenceProbation" + cmaRule.getAgeOfSentenceProbation() + " op" + cmaRule.getAgeOfSentenceProbationOperator());
		setOperator(cmaRule.getAgeOfSentenceProbationOperator());
		
		Predicate<CrimCharge> catPredicate;
		switch (paramOperatorEnum){
		
		case GTHAN:
			
			 catPredicate = p -> {
				 String age = p.getAgeofSentenceProbation();
				 Double ruleAge = cmaRule.getAgeOfSentenceProbation();
				 return (age!= null &&  ruleAge!=null && Double.parseDouble(p.getAgeofSentenceProbation())  > cmaRule.getAgeOfSentenceProbation().doubleValue()); 
			 };
		    
		     break;
		case GETHAN:
			
			catPredicate = p -> {
				String age = p.getAgeofSentenceProbation();
				Double ruleAge = cmaRule.getAgeOfSentenceProbation();
				return (age!= null &&  ruleAge!=null && Double.parseDouble(p.getAgeofSentenceProbation())  >= cmaRule.getAgeOfSentenceProbation().doubleValue());
			};
		    
		     break;
		case LTHAN:
			
			catPredicate = p -> {
				String age = p.getAgeofSentenceProbation();
				Double ruleAge = cmaRule.getAgeOfSentenceProbation();
				return (age!= null &&  ruleAge!=null && Double.parseDouble(p.getAgeofSentenceProbation())  < cmaRule.getAgeOfSentenceProbation().doubleValue());
			};
			 
		     break;
		case LETHAN:
			catPredicate = p -> {
				String age = p.getAgeofSentenceProbation();
				Double ruleAge = cmaRule.getAgeOfSentenceProbation();
				return (age!= null &&  ruleAge!=null && Double.parseDouble(p.getAgeofSentenceProbation())  <= cmaRule.getAgeOfSentenceProbation().doubleValue());
			};
			
			
		     break;		     
		case EQ:
			catPredicate = p -> {
				String age = p.getAgeofSentenceProbation();
				Double ruleAge = cmaRule.getAgeOfSentenceProbation();
				return (age!= null &&  ruleAge!=null && Double.parseDouble(p.getAgeofSentenceProbation())  == cmaRule.getAgeOfSentenceProbation().doubleValue());
			};
			
		     break;		     

		default:
			catPredicate = p-> true;
		}
		filters.add(catPredicate);
		
	}

}
