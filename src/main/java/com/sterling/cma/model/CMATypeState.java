package com.sterling.cma.model;

import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

public class CMATypeState extends AbstractCMAType {

	public CMATypeState(CMAType inner) {
		super(inner);
		// TODO Auto-generated constructor stub
	}

	public Collection<Predicate<CrimCharge>> score(Collection<Predicate<CrimCharge>> filters,
			Collection<Predicate<CrimCharge>> exceptionfilters,
			Collection<Predicate<CrimCharge>> groupChargelevelFilter, Collection<Predicate<CrimCharge>> dispositionFilter,Collection<Predicate<CrimCharge>> stateFilter,CmaRule cmaRule) {

		super.score(filters, exceptionfilters, groupChargelevelFilter,dispositionFilter,stateFilter, cmaRule);
		doState(filters, exceptionfilters,stateFilter,cmaRule);
		return null;

	}

	private void doState(Collection<Predicate<CrimCharge>> filters,
			Collection<Predicate<CrimCharge>> exceptionfilters,
			Collection<Predicate<CrimCharge>> stateFilter, CmaRule cmaRule) {
		// TODO Auto-generated method stub

		/*
		 * Predicate<CrimCharge> dispPredicate = p->{ String dispositionPredicate =
		 * p.getState(); return dispositionPredicate!= null &&
		 * p.getState().equalsIgnoreCase(cmaRule.getState()); };
		 * filters.add(dispPredicate);
		 */

		if (cmaRule.getExceptState() == null || cmaRule.getExceptState().isEmpty()) {
			if (cmaRule.getState() != null) {
				String[] States = cmaRule.getState().split(",");
				for (String state : States) {
					Predicate<CrimCharge> dispPredicate = p -> {
						String dispositionPredicate = p.getState();
						return dispositionPredicate != null && p.getState().equalsIgnoreCase(state.trim());
					};
					stateFilter.add(dispPredicate);
				}
			}
			//filters.add(dispPredicate);

		} else {
			List<String> exDisp = cmaRule.getExceptState();
			for (String cat : exDisp) {

				Predicate<CrimCharge> dispPredicate = p -> p.getState().equalsIgnoreCase(cat.trim());
				exceptionfilters.add(dispPredicate);

			}

		}

	}

}
