package com.sterling.cma.model;

import com.sterling.cma.model.enums.ParamOperatorEnum;

public class CustomerParamValueMapper {
	private Long customerParamValueMapperId;
	private ParamGroupMaster paramGroupMaster;
	private String paramValue;
	private String paramLogicalValue;
	private ParamOperatorEnum paramOperatorEnum;
	private CustomerRuleConfiguration customerRuleConfiguration;

	public Long getCustomerParamValueMapperId() {
		return customerParamValueMapperId;
	}

	public void setCustomerParamValueMapperId(Long customerParamValueMapperId) {
		this.customerParamValueMapperId = customerParamValueMapperId;
	}

	public ParamGroupMaster getParamGroupMaster() {
		return paramGroupMaster;
	}

	public void setParamGroupMaster(ParamGroupMaster paramGroupMaster) {
		this.paramGroupMaster = paramGroupMaster;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public String getParamLogicalValue() {
		return paramLogicalValue;
	}

	public void setParamLogicalValue(String paramLogicalValue) {
		this.paramLogicalValue = paramLogicalValue;
	}

	public ParamOperatorEnum getParamOperatorEnum() {
		return paramOperatorEnum;
	}

	public void setParamOperatorEnum(ParamOperatorEnum paramOperatorEnum) {
		this.paramOperatorEnum = paramOperatorEnum;
	}

	public CustomerRuleConfiguration getCustomerRuleConfiguration() {
		return customerRuleConfiguration;
	}

	public void setCustomerRuleConfiguration(CustomerRuleConfiguration customerRuleConfiguration) {
		this.customerRuleConfiguration = customerRuleConfiguration;
	}
}
