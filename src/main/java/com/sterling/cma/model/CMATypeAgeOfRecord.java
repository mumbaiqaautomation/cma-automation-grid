package com.sterling.cma.model;


import java.util.Collection;
import java.util.function.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sterling.cma.model.enums.ParamOperatorEnum;


public class CMATypeAgeOfRecord extends AbstractCMAType{

	
	private static Logger logger = LoggerFactory.getLogger(CMATypeAgeOfRecord.class);
	
	public CMATypeAgeOfRecord(CMAType inner) {
		super(inner);
		// TODO Auto-generated constructor stub
	}
	
	public Collection<Predicate<CrimCharge>>  score(Collection<Predicate<CrimCharge>> filters,Collection<Predicate<CrimCharge>> exceptionfilters,
			Collection<Predicate<CrimCharge>> groupChargelevelFilter,Collection<Predicate<CrimCharge>> dispositionFilter,Collection<Predicate<CrimCharge>> stateFilter,CmaRule cmaRule) {
		super.score(filters,exceptionfilters,groupChargelevelFilter,dispositionFilter,stateFilter,cmaRule);
		doAgeOfRecord(filters,cmaRule);
		return null;
		
	}

	private void doAgeOfRecord(Collection<Predicate<CrimCharge>> filters,CmaRule cmaRule) {
		 
		
		setOperator(cmaRule.getAgeOfRecordOperator());
		
		//logger.info("setting filter for Age of Record"+ cmaRule.getAgeOfRecord() +" op " + cmaRule.getAgeOfRecordOperator());
		
		Predicate<CrimCharge> catPredicate;
		switch (paramOperatorEnum){
		
		case GTHAN:
			
			
		     catPredicate = p -> {
				 String age = p.getAgeOfRecord();
				 Double ruleAge = cmaRule.getAgeOfRecord();
				 return (age!= null &&  ruleAge!=null && Double.parseDouble(p.getAgeOfRecord())  > cmaRule.getAgeOfRecord().doubleValue());
		     
			 };//n
		     break;
		case GETHAN:
			
			catPredicate = p -> {
				 String age = p.getAgeOfRecord();
				 Double ruleAge = cmaRule.getAgeOfRecord();
				 return (age!= null &&  ruleAge!=null && Double.parseDouble(p.getAgeOfRecord())  >= cmaRule.getAgeOfRecord().doubleValue());
		     
			 };//n
			
		     break;
		case LTHAN:
			
			
			 catPredicate = p -> {
				 String age = p.getAgeOfRecord();
				 Double ruleAge = cmaRule.getAgeOfRecord();
				 return (age!= null &&  ruleAge!=null && Double.parseDouble(p.getAgeOfRecord())  < cmaRule.getAgeOfRecord().doubleValue());
		     
			 };
		     break;
		case LETHAN:
			
			
			catPredicate = p ->{
				
				String age = p.getAgeOfRecord();
				Double ruleAge = cmaRule.getAgeOfRecord();
				
				 return (age!= null && ruleAge!=null && Double.parseDouble(age)  <= cmaRule.getAgeOfRecord().doubleValue());
		     
			};
			
		   
		     break;		     
		case EQ:
			
			
catPredicate = p ->{
				
				String age = p.getAgeOfRecord();
				Double ruleAge = cmaRule.getAgeOfRecord();
				 return (age!= null &&   ruleAge!=null && Double.parseDouble(p.getAgeOfRecord())  == cmaRule.getAgeOfRecord().doubleValue());
		     
			};
			
		     break;		     

		default:
			
			catPredicate = p ->{
				
				String age = p.getAgeOfRecord();
				Double ruleAge = cmaRule.getAgeOfRecord();
				 return (age!= null &&  ruleAge!=null && Double.parseDouble(p.getAgeOfRecord())  >= 0);
		     
			};
			
		}
		filters.add(catPredicate);
		
	}

}
