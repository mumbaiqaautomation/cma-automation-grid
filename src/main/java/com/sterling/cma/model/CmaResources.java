package com.sterling.cma.model;

public class CmaResources {
	private Long cmaResourcesId;
	private String state;
	private CmaRequest cmaRequest;
	private CmaResponse cmaResponse;
	private String cmaTrackingId;

	public String getCmaTrackingId() {
		return cmaTrackingId;
	}

	public void setCmaTrackingId(String cmaTrackingId) {
		this.cmaTrackingId = cmaTrackingId;
	}

	public Long getCmaResourcesId() {
		return cmaResourcesId;
	}

	public void setCmaResourcesId(Long cmaResourcesId) {
		this.cmaResourcesId = cmaResourcesId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public CmaRequest getCmaRequest() {
		return cmaRequest;
	}

	public void setCmaRequest(CmaRequest cmaRequest) {
		this.cmaRequest = cmaRequest;
	}

	public CmaResponse getCmaResponse() {
		return cmaResponse;
	}

	public void setCmaResponse(CmaResponse cmaResponse) {
		this.cmaResponse = cmaResponse;
	}

}



