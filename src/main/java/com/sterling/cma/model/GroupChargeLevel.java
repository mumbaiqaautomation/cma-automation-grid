package com.sterling.cma.model;

import java.util.HashSet;
import java.util.Set;

import org.springframework.util.CollectionUtils;

public class GroupChargeLevel {
	private String groupChargeLevelName;
	private Set<GroupChargeLevel> groupChargeLevelList;
	private Set<String> chargeLevelList;

	public String getGroupChargeLevelName() {
		return groupChargeLevelName;
	}

	public void setGroupChargeLevelName(String groupChargeLevelName) {
		this.groupChargeLevelName = groupChargeLevelName;
	}

	public Set<GroupChargeLevel> getGroupChargeLevelList() {
		if(CollectionUtils.isEmpty(groupChargeLevelList)){
			groupChargeLevelList = new HashSet<>();
		}
		return groupChargeLevelList;
	}

	public void setGroupChargeLevelList(Set<GroupChargeLevel> groupChargeLevelList) {
		this.groupChargeLevelList = groupChargeLevelList;
	}

	public Set<String> getChargeLevelList() {
		if(CollectionUtils.isEmpty(chargeLevelList)){
			chargeLevelList = new HashSet<>();
		}
		return chargeLevelList;
	}

	public void setChargeLevelList(Set<String> chargeLevelList) {
		this.chargeLevelList = chargeLevelList;
	}

}
