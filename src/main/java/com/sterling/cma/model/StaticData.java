package com.sterling.cma.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "to-be-picked-from-propertyfile")
public class StaticData {

	private String staticKey;
	private String staticValue;

	@DynamoDBHashKey(attributeName = "static-key")
	public String getStaticKey() {
		return staticKey;
	}

	public void setStaticKey(String staticKey) {
		this.staticKey = staticKey;
	}

	@DynamoDBAttribute(attributeName = "static-value")
	public String getStaticValue() {
		return staticValue;
	}

	public void setStaticValue(String staticValue) {
		this.staticValue = staticValue;
	}

}
