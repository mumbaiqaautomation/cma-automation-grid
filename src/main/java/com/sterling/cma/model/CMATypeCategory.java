package com.sterling.cma.model;

import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CMATypeCategory extends CMAType {

	private static Logger logger = LoggerFactory.getLogger(CMATypeCategory.class);

	@Override
	public Collection<Predicate<CrimCharge>> score(Collection<Predicate<CrimCharge>> filters,
			Collection<Predicate<CrimCharge>> exceptionfilters, 
			Collection<Predicate<CrimCharge>> groupChargelevelFilter,
			Collection<Predicate<CrimCharge>> dispositionFilter,Collection<Predicate<CrimCharge>> stateFilter,
			CmaRule cmaRule) {
		// TODO Auto-generated method stub

		// loop through all the list and then negate and send back for except categories
		//logger.info("setting chargecategory filter" + cmaRule.getChargeCategory());

		if (cmaRule.getExceptCategory() == null || cmaRule.getExceptCategory().isEmpty()) {
			if (cmaRule.getRuleType().contains("ChargeCategory")) {
				
				Predicate<CrimCharge> catPredicate = p -> {
					String category = p.getChargeCategory();
					return category!=null && p.getChargeCategory().equalsIgnoreCase(cmaRule.getChargeCategory());
				};
						
				
				filters.add(catPredicate);
			} else {

				Predicate<CrimCharge> catPredicate = p -> true;
				filters.add(catPredicate);
			}
		} else {
			List<String> exCat = cmaRule.getExceptCategory();
			for (String cat : exCat) {

				Predicate<CrimCharge> catPredicate = p -> p.getChargeCategory().equalsIgnoreCase(cat);
				exceptionfilters.add(catPredicate);

			}

		}

		return null;
	}

	public CMATypeCategory() {

	}

	// hack for all categories
	public CMATypeCategory(String category) {

	}

}
