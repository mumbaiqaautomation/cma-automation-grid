package com.sterling.cma.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "cma-consicutive-counter")
public class ConsicutiveCounter {

	private String counterType;
	private String counterLevelOne;
	private String counterLevelTwo;
	private String counterLevelThree;

	@DynamoDBHashKey(attributeName = "counter-type")
	public String getCounterType() {
		return counterType;
	}

	public void setCounterType(String counterType) {
		this.counterType = counterType;
	}

	public void setCounterLevelThree(String counterLevelThree) {
		this.counterLevelThree = counterLevelThree;
	}

	@DynamoDBAttribute(attributeName = "counter-level-one")
	public String getCounterLevelOne() {
		return counterLevelOne;
	}

	public void setCounterLevelOne(String counterLevelOne) {
		this.counterLevelOne = counterLevelOne;
	}

	@DynamoDBAttribute(attributeName = "counter-level-two")
	public String getCounterLevelTwo() {
		return counterLevelTwo;
	}

	public void setCounterLevelTwo(String counterLevelTwo) {
		this.counterLevelTwo = counterLevelTwo;
	}

	@DynamoDBAttribute(attributeName = "counter-level-three")
	public String getCounterLevelThree() {
		return counterLevelThree;
	}

	@Override
	public String toString() {
		return "ConsicutiveCounter [counterType=" + counterType + ", counterLevelOne=" + counterLevelOne
				+ ", counterLevelTwo=" + counterLevelTwo + ", counterLevelThree=" + counterLevelThree + "]";
	}	
	
}
