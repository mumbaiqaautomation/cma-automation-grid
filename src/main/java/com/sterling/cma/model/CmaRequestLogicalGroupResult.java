package com.sterling.cma.model;

import com.sterling.cma.model.enums.CmaDecissionLevelEnum;

public class CmaRequestLogicalGroupResult {
	private Long cmaRequestLogicalGroupResultId;
	private CustomerLogicalOperationGroup customerLogicalOperationGroup;
	private CmaDecissionLevelEnum cmaDecissionLevelEnum;
	private CmaRequest cmaRequest;
	private Boolean result;
	private String chargeIdentifier;

	public Long getCmaRequestLogicalGroupResultId() {
		return cmaRequestLogicalGroupResultId;
	}

	public void setCmaRequestLogicalGroupResultId(Long cmaRequestLogicalGroupResultId) {
		this.cmaRequestLogicalGroupResultId = cmaRequestLogicalGroupResultId;
	}

	public CustomerLogicalOperationGroup getCustomerLogicalOperationGroup() {
		return customerLogicalOperationGroup;
	}

	public void setCustomerLogicalOperationGroup(CustomerLogicalOperationGroup customerLogicalOperationGroup) {
		this.customerLogicalOperationGroup = customerLogicalOperationGroup;
	}

	public CmaDecissionLevelEnum getCmaDecissionLevelEnum() {
		return cmaDecissionLevelEnum;
	}

	public void setCmaDecissionLevelEnum(CmaDecissionLevelEnum cmaDecissionLevelEnum) {
		this.cmaDecissionLevelEnum = cmaDecissionLevelEnum;
	}

	public CmaRequest getCmaRequest() {
		return cmaRequest;
	}

	public void setCmaRequest(CmaRequest cmaRequest) {
		this.cmaRequest = cmaRequest;
	}

	public Boolean getResult() {
		return result;
	}

	public void setResult(Boolean result) {
		this.result = result;
	}

	public String getChargeIdentifier() {
		return chargeIdentifier;
	}

	public void setChargeIdentifier(String chargeIdentifier) {
		this.chargeIdentifier = chargeIdentifier;
	}

}
