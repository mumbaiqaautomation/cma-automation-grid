package com.sterling.cma.model;


import java.util.Collection;
import java.util.function.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sterling.cma.model.enums.ParamOperatorEnum;

public class CMATypeAgeOfDefendent extends AbstractCMAType{
	
	private static Logger logger = LoggerFactory.getLogger(CMATypeAgeOfDefendent.class);

	public CMATypeAgeOfDefendent(CMAType inner) {
		super(inner);
		// TODO Auto-generated constructor stub
	}
	
	public Collection<Predicate<CrimCharge>>  score(Collection<Predicate<CrimCharge>> filters,Collection<Predicate<CrimCharge>> exceptionfilters,
			Collection<Predicate<CrimCharge>> groupChargelevelFilter,Collection<Predicate<CrimCharge>> dispositionFilter,Collection<Predicate<CrimCharge>> stateFilter,
			CmaRule cmaRule) {
		super.score(filters,exceptionfilters,groupChargelevelFilter,dispositionFilter,stateFilter,cmaRule);
		doAgeOfDefendant(filters,cmaRule);
		return null;
		
	}

	private void doAgeOfDefendant(Collection<Predicate<CrimCharge>> filters,CmaRule cmaRule) {
		
		//logger.info("setting filter for AgeofSentence" + cmaRule.getAgeOfDefendant() + " op" + cmaRule.getAgeOfDefendantOperator());
		setOperator(cmaRule.getAgeOfDefendantOperator());
		
		Predicate<CrimCharge> catPredicate;
		switch (paramOperatorEnum){
		
		case GTHAN:
			
			 catPredicate = p -> 
		     Integer.parseInt(p.getDefendantAge())  > cmaRule.getAgeOfDefendant().intValue();//n
		     break;
		case GETHAN:
			
			catPredicate = p -> 
		     Integer.parseInt(p.getDefendantAge())  >= cmaRule.getAgeOfDefendant().intValue();
		     break;
		case LTHAN:
			
			 catPredicate = p -> 
		     Integer.parseInt(p.getDefendantAge())  < cmaRule.getAgeOfDefendant().intValue();//n
		     break;
		case LETHAN:
			
			catPredicate = p -> 
		     Integer.parseInt(p.getDefendantAge())  <= cmaRule.getAgeOfDefendant().intValue();
		     break;		     
		case EQ:
			
			catPredicate = p -> 
		     Integer.parseInt(p.getDefendantAge())  == cmaRule.getAgeOfDefendant().intValue();
		     break;		     

		default:
			catPredicate = p-> Integer.parseInt(p.getDefendantAge())  >= 0;
		}
		filters.add(catPredicate);
		
	}

}
