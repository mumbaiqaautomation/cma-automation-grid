package com.sterling.cma.model;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "to-be-picked-from-propertyfile")
public class CmaRuleSetting {
	private String rule_id;
	private String frequency;
	private String ageOfRecord;
	private String ageOfSentence;
	/*
	 * @DynamoDBAttribute(attributeName = "static-value") private String
	 * frequencyGroupId;
	 */
	private String score;
	private String cmaTier;
	private List<String> exceptCategory = new ArrayList<String>();
	private List<String> exceptDispostion = new ArrayList<String>();
	private List<String> exceptState = new ArrayList<String>();
	private List<String> cmaType = new ArrayList<String>();
	private String ageOfDefendant;
	private String ageOfDefendantOperator;
	private List<String> chargeLevel;
	private List<String> dispositionType;
	private String chargeCategory;
	private String warrantStatus;
	private String probationStatus;
	private String frequencyOperator;
	private String disposition;
	private String ageOfSentenceProbation;
	private String sentenceStatus;
	private String ageOfRecordOperator;
	private String ageOfSentenceProbationOperator;
	private String ageOfSentenceOperator;
	//private String categoryLevel;
	private List<String> state;

	@DynamoDBAttribute(attributeName = "frequency_operator")
	public String getFrequencyOperator() {
		return frequencyOperator;
	}

	public void setFrequencyOperator(String frequencyOperator) {
		this.frequencyOperator = frequencyOperator;
	}

	@DynamoDBAttribute(attributeName = "age_of_defendant_operator")
	public String getAgeOfDefendantOperator() {
		return ageOfDefendantOperator;
	}

	public void setAgeOfDefendantOperator(String ageOfDefendantOperator) {
		this.ageOfDefendantOperator = ageOfDefendantOperator;
	}

	@DynamoDBAttribute(attributeName = "age_of_defendant")
	public String getAgeOfDefendant() {
		return ageOfDefendant;
	}

	public void setAgeOfDefendant(String ageOfDefendant) {
		this.ageOfDefendant = ageOfDefendant;
	}

	@DynamoDBAttribute(attributeName = "except_state")
	public List<String> getExceptState() {
		return exceptState;
	}

	public void setExceptState(List<String> exceptState) {
		this.exceptState = exceptState;
	}

	@DynamoDBAttribute(attributeName = "except_dispostion")
	public List<String> getExceptDispostion() {
		return exceptDispostion;
	}

	public void setExceptDispostion(List<String> exceptDispostion) {
		this.exceptDispostion = exceptDispostion;
	}

	public void setProbationStatus(String probationStatus) {
		this.probationStatus = probationStatus;
	}

	@DynamoDBAttribute(attributeName = "age_of_sentence_probation")
	public String getAgeOfSentenceProbation() {
		return ageOfSentenceProbation;
	}

	public void setAgeOfSentenceProbation(String ageOfSentenceProbation) {
		this.ageOfSentenceProbation = ageOfSentenceProbation;
	}

	@DynamoDBAttribute(attributeName = "sentence_active")
	public String getSentenceStatus() {
		return sentenceStatus;
	}

	public void setSentenceStatus(String sentenceStatus) {
		this.sentenceStatus = sentenceStatus;
	}

	@DynamoDBAttribute(attributeName = "disposition")
	public String getDisposition() {
		return disposition;
	}

	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}

	@DynamoDBAttribute(attributeName = "cma_type")
	public List<String> getCmaType() {
		return cmaType;
	}

	public void setCmaType(List<String> cmaType) {
		this.cmaType = cmaType;
	}

	@DynamoDBAttribute(attributeName = "age_of_sentence_probation_operator")
	public String getAgeOfSentenceProbationOperator() {
		return ageOfSentenceProbationOperator;
	}

	public void setAgeOfSentenceProbationOperator(String ageOfSentenceProbationOperator) {
		this.ageOfSentenceProbationOperator = ageOfSentenceProbationOperator;
	}

	@DynamoDBAttribute(attributeName = "age_of_sentence_operator")
	public String getAgeOfSentenceOperator() {
		return ageOfSentenceOperator;
	}

	public void setAgeOfSentenceOperator(String ageOfSentenceOperator) {
		this.ageOfSentenceOperator = ageOfSentenceOperator;
	}

	@DynamoDBAttribute(attributeName = "age_of_record_operator")
	public String getAgeOfRecordOperator() {
		return ageOfRecordOperator;
	}

	public void setAgeOfRecordOperator(String ageOfRecordOperator) {
		this.ageOfRecordOperator = ageOfRecordOperator;
	}

	@DynamoDBAttribute(attributeName = "charge_level")
	public List<String> getChargeLevel() {
		return chargeLevel;
	}

	public void setChargeLevel(List<String> chargeLevel) {
		this.chargeLevel = chargeLevel;
	}

	@DynamoDBAttribute(attributeName = "disposition_type")
	public List<String> getDispositionType() {
		return dispositionType;
	}

	public void setDispositionType(List<String> dispositionType) {
		this.dispositionType = dispositionType;
	}

	@DynamoDBAttribute(attributeName = "charge_category")
	public String getChargeCategory() {
		return chargeCategory;
	}

	public void setChargeCategory(String chargeCategory) {
		this.chargeCategory = chargeCategory;
	}

	@DynamoDBAttribute(attributeName = "warrant_active")
	public String getWarrantStatus() {
		return warrantStatus;
	}

	public void setWarrantStatus(String warrantStatus) {
		this.warrantStatus = warrantStatus;
	}

	/*@DynamoDBAttribute(attributeName = "category_level")
	public String getCategoryLevel() {
		return categoryLevel;
	}

	public void setCategoryLevel(String categoryLevel) {
		this.categoryLevel = categoryLevel;
	}*/

	@DynamoDBAttribute(attributeName = "state")
	public List<String> getState() {
		return state;
	}

	public void setState(List<String> state) {
		this.state = state;
	}

	@DynamoDBAttribute(attributeName = "cma_score")
	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	@DynamoDBRangeKey(attributeName = "rule_id")
	public String getRuleId() {
		return rule_id;
	}

	public void setRuleId(String ruleId) {
		this.rule_id = ruleId;
	}

	@DynamoDBAttribute(attributeName = "frequency")
	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	@DynamoDBAttribute(attributeName = "age_of_record")
	public String getAgeOfRecord() {
		return ageOfRecord;
	}

	public void setAgeOfRecord(String ageOfRecord) {
		this.ageOfRecord = ageOfRecord;
	}

	@DynamoDBAttribute(attributeName = "age_of_sentence")
	public String getAgeOfSentence() {
		return ageOfSentence;
	}

	public void setAgeOfSentence(String ageOfSentence) {
		this.ageOfSentence = ageOfSentence;
	}

	@DynamoDBAttribute(attributeName = "cma_tier")
	public String getCmaTier() {
		return cmaTier;
	}

	public void setCmaTier(String cmaTier) {
		this.cmaTier = cmaTier;
	}

	/**
	 * @return the exceptCategory
	 */
	@DynamoDBAttribute(attributeName = "except_category")
	public List<String> getExceptCategory() {
		return exceptCategory;
	}

	/**
	 * @param exceptCategory
	 *            the exceptCategory to set
	 */
	public void setExceptCategory(List<String> exceptCategory) {
		this.exceptCategory = exceptCategory;
	}

	@DynamoDBAttribute(attributeName = "probation_status")
	public String getProbationStatus() {
		// TODO Auto-generated method stub
		return this.probationStatus;
	}
}