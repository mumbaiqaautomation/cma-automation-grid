package com.sterling.cma.model;

import java.util.List;

public class CmaRuleRequest {
	
	private String modelId;
	private List<CmaRuleSetting> cmaRules;
	public String getModelId() {
		return modelId;
	}
	public void setModelId(String modelId) {
		this.modelId = modelId;
	}
	public List<CmaRuleSetting> getCmaRules() {
		return cmaRules;
	}
	public void setCmaRules(List<CmaRuleSetting> cmaRules) {
		this.cmaRules = cmaRules;
	}

}
